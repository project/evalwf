<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElement.php - The Annotation Plugin ty definition
*
* @author Tóthpál István <istvan@tothpal.eu>
*
* @references: Jennifer Hodgdon: Programmer's Guide to Drupal
*/

namespace Drupal\evalwf\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EvalWFElement annotation object.
 *
 * @Annotation
 */
class EvalWFElement extends Plugin {

  public $id;
  public $label = '';
  public $types = '';

}
