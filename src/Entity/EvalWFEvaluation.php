<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Entity\EvalWFEvaluation.php - The Evaluation Entity
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * @ConfigEntityType(
 *   id = "evalwf_evaluation",
 *   label = @Translation("EvalWF evaluation"),
 *   handlers = {
 *     "list_builder" = "Drupal\evalwf\Controller\EvalWFEvaluationListBuilder",
 *     "form" = {
 *       "view" = "Drupal\evalwf\Form\EvalWFEvaluationForm",
 *       "list" = "Drupal\evalwf\Form\EvalWFEvaluationForm",
 *       "result" = "Drupal\evalwf\Form\EvalWFEvaluationForm",
 *       "delete" = "Drupal\evalwf\Form\EvalWFEvaluationDeleteForm",
 *     },
 *   },
 *   config_prefix = "evalwf_evaluation",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "wfid" = "wfid",
 *     "sid" = "sid",
 *     "total_points" = "total_points",
 *     "elements_points" = "elements_points",
 *     "conditions_data" = "conditions_data",
 *     "sent" = "sent",
 *     "sentto" = "sentto",
 *     "timestamp" = "timestamp",
 *     "uid" = "uid",
 *     "checksum" = "checksum",
 *   },
 *   config_export = {
 *     "id",
 *     "wfid",
 *     "sid",
 *     "total_points",
 *     "elements_points",
 *     "conditions_data",
 *     "sent",
 *     "sentto",
 *     "timestamp",
 *     "uid",
 *     "checksum",
 *   },
 *   links = {
 *     "view-form" = "/admin/structure/evalwf/evaluations/view/{evalwf_evaluation}",
 *     "result-form" = "/admin/structure/evalwf/evaluations/result/{evalwf_evaluation}",
 *     "delete-form" = "/admin/structure/evalwf/evaluations/delete/{evalwf_evaluation}",
 *     "list-form" = "/admin/structure/evalwf/evaluations",
 *   }
 * )
 */

class EvalWFEvaluation extends ConfigEntityBase implements ConfigEntityInterface {

  protected $id;
  protected $wfid;
  protected $sid;

  protected $total_points;
  protected $elements_points;
  protected $conditions_data;

  protected $sent;
  protected $sentto;
  protected $timestamp;
  protected $uid;

  protected $checksum;

  protected $ismodified = false;

  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
    $this->setModified();
  }

  public function getId() {
    return $this->id;
  }

  public function getWfId() {
    return $this->wfid;
  }

  public function getSId() {
    return $this->sid;
  }

  public function getTotal_Points() {
    return $this->total_points;
  }

  public function getElements_Points() {
    return $this->elements_points;
  }

  public function getConditions_Data() {
    return $this->conditions_data;
  }

  public function isSent() {
    return $this->sent;
  }
  public function setSent( $b ) {
    $this->sent = $b;
    $this->setModified();
  }

  public function getSentTo() {
    return $this->sentto;
  }
  public function setSentTo( $s ) {
    $this->sentto = $s;
    $this->setModified();
  }

  public function getTimestamp() {
    return $this->timestamp;
  }

  public function getUserId() {
    return $this->uid;
  }

  public function getUserName() {
    return \Drupal\user\Entity\User::load($this->getUserId())->getAccountName();
  }

  public function getCheckSum() {
    return unserialize( $this->checksum );
  }
  public function setCheckSum( $checksum ) {
    $this->checksum = serialize( $checksum );
    $this->setModified();
  }

  public function isModified() {
    return $this->ismodified;
  }
  public function setModified() {
    $this->ismodified = true;
  }
  public function clearModified() {
    $this->ismodified = false;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    parent::getCacheTags();
    $cache_tags[] = 'config:evalwf.evalwf_evaluation.' . $this->getId();
    $cache_tags[] = 'config:evalwf_evaluation_list';
    return $cache_tags;
  }

  /**
   *    Saves evaluation entity.
   */
  public function save() {
    if ($this->isModified()) {
      parent::save();
      Cache::invalidateTags($this->getCacheTags());
      $this->clearModified();
    }
  }

}
?>
