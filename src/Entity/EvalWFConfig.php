<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Entity\EvalWFConfig.php - The Config Entity
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * @ConfigEntityType(
 *   id = "evalwf_config",
 *   label = @Translation("EvalWF config"),
 *   handlers = {
 *     "list_builder" = "Drupal\evalwf\Controller\EvalWFConfigListBuilder",
 *     "form" = {
 *       "settings" = "Drupal\evalwf\Form\EvalWFConfigForm",
 *     }
 *   },
 *   config_prefix = "evalwf_config",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "adminsendmail" = "adminsendmail",
 *     "evalasuser" = "evalasuser",
 *     "mailer" = "mailer",
 *     "sendername" = "sendername",
 *     "sender" = "sender",
 *     "subject" = "subject",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "adminsendmail",
 *     "evalasuser",
 *     "mailer",
 *     "sendername",
 *     "sender",
 *     "subject",
 *   },
 *   links = {
 *     "settings-form" = "/admin/structure/evalwf/config",
 *   }
 * )
 */

class EvalWFConfig extends ConfigEntityBase implements ConfigEntityInterface {

  protected $id;
  protected $label;

  protected $adminsendmail;
  protected $evalasuser;

  protected $mailer;
  protected $sendername;
  protected $sender;
  protected $subject;

  public function getId() {
    return $this->id;
  }
  public function setId( $id ) {
    $this->id = 'evalwf_config';
  }

  public function getLabel() {
    return $this->label;
  }
  public function setLabel( $l ) {
    $this->label = 'evalwf_config';
  }

  /*
   *  Returns if we have to send emails when administer evaluates
   */
  public function isAdminSendMail() {
    return $this->adminsendmail;
  }
  public function setAdminSendMail( $b ) {
    $this->adminsendmail = $b;
  }

  /*
   *  Returns if we have to create evaluation with the user who submitted the form when administer evaluates
   */
  public function isEvalAsUser() {
    return $this->evalasuser;
  }
  public function setEvalAsUser( $b ) {
    $this->evalasuser = $b;
  }

  /*
   *  Returns the current mail sender ( DrupalMail / PHPMail )
   */
  public function getMailer() {
    return $this->mailer;
  }
  public function setMailer( $m ) {
    if ( in_array( $m, array_keys($this->getAvailableMailers()) ) )
      $this->mailer = $m;
    else
      $this->mailer = key($this->getAvailableMailers());
  }
  public function getAvailableMailers() {
    return array(
        'drupalmail' => 'drupalmail',
        'phpmail' => 'phpmail',
      );
  }

  /*
   *  Returns the display name of the sender of the email
   */
  public function getSenderName() {
    return $this->sendername;
  }
  public function setSenderName( $s ) {
    $this->sendername = $s;
  }

  /*
   *  Returns the email address of the sender
   */
  public function getSender() {
    return $this->sender;
  }
  public function setSender( $s ) {
    $this->sender = $s;
  }

  /*
   *  Returns the subject of the email that will be sent
   */
  public function getSubject() {
    return $this->subject;
  }
  public function setSubject( $s ) {
    $this->subject = $s;
  }

    /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    parent::getCacheTags();
    $cache_tags[] = 'config:evalwf.evalwf_config.' . $this->getId();
    $cache_tags[] = 'config:evalwf_config_list';
//    $cache_tags[] = 'config:evalwf.settings';
    return $cache_tags;
  }

  /**
   *    Saves config entity
   */
  public function save() {
    parent::save();
    Cache::invalidateTags($this->getCacheTags());
  }

}
?>
