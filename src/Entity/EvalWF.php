<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Entity\EvalWF.php - The Entity
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;
use Drupal\Core\Cache\Cache;
//use Drupal\evalwf\EvalWFInterface;

/**
 * @ConfigEntityType(
 *   id = "evalwf",
 *   label = @Translation("Evaluate Webform"),
 *   handlers = {
 *     "list_builder" = "Drupal\evalwf\Controller\EvalWFListBuilder",
 *     "form" = {
 *       "add" = "Drupal\evalwf\Form\EvalWFForm",
 *       "settings" = "Drupal\evalwf\Form\EvalWFForm",
 *       "general_settings" = "Drupal\evalwf\Form\EvalWFForm",
 *       "elements_settings" = "Drupal\evalwf\Form\EvalWFForm",
 *       "conditions_settings" = "Drupal\evalwf\Form\EvalWFForm",
 *       "delete" = "Drupal\evalwf\Form\EvalWFDeleteForm",
 *     }
 *   },
 *   config_prefix = "evalwf",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "webform_id" = "webform_id",
 *     "evaltitle" = "evaltitle",
 *     "evalintro" = "evalintro",
 *     "directjump" = "directjump",
 *     "showevalpage" = "showevalpage",
 *     "showall" = "showall",
 *     "renderasform" = "renderasform",
 *     "includetitle" = "includetitle",
 *     "includetitleinemail" = "includetitleinemail",
 *     "sendinemail" = "sendinemail",
 *     "email" = "email",
 *     "sendtouser" = "sendtouser",
 *     "emailrenderasform" = "emailrenderasform",
 *     "elements_data" = "elements_data",
 *     "conditions_data" = "conditions_data",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "webform_id",
 *     "evaltitle",
 *     "evalintro",
 *     "directjump",
 *     "showevalpage",
 *     "showall",
 *     "renderasform",
 *     "includetitle",
 *     "includetitleinemail",
 *     "sendinemail",
 *     "email",
 *     "sendtouser",
 *     "emailrenderasform",
 *     "elements_data",
 *     "conditions_data",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/evalwf/add",
 *     "settings-form" = "/admin/structure/evalwf/{webform}",
 *     "delete-form" = "/admin/structure/evalwf/delete/{evalwf}",
 *   }
 * )
 */

class EvalWF extends ConfigEntityBase implements ConfigEntityInterface { //EvalWFInterface {
  protected $id;
  protected $label;
  protected $webform_id;

  protected $evaltitle = 'Results of Your submission.';
  protected $evalintro = 'Thanks for filling. See Your results below.';
  protected $directjump = false;
  protected $showevalpage = true;
  protected $showall = false;
  protected $renderasform = true;
  protected $includetitle = true;

  protected $includetitleinemail = true;
  protected $sendinemail = false;
  protected $email;
  protected $sendtouser = false;
  protected $emailrenderasform = true;

  protected $elements_data;
  protected $conditions_data;

  public function setId( $newid ) {
    $this->id = $newid;
  }
  public function setLabel( $newlabel ) {
    $this->label = $newlabel;
  }

  public function getId() {
    return $this->id;
  }
  public function getLabel() {
    return $this->label;
  }

  public function getEvaluationTitle() {
    return $this->evaltitle;
  }
  public function setEvaluationTitle( $t ) {
    $this->evaltitle = $t;
  }

  public function getEvaluationIntro() {
    return $this->evalintro;
  }
  public function setEvaluationIntro( $t ) {
    $this->evalintro = $t;
  }

  /*
   *  Returns the ID of the connected webform
   */
  public function getWebformId() {
    return $this->webform_id;
  }
  public function setWebformId( $wid ) {
    $this->webform_id = $wid;
  }

  public function isDirectJump() {
    return $this->directjump;
  }
  public function setDirectJump( $b ) {
    $this->directjump = $b;
  }

  public function isShowEvaluationPage() {
    return $this->showevalpage;
  }
  public function setShowEvaluationPage( $b ) {
    $this->showevalpage = $b;
  }

  public function isShowAll() {
    return $this->showall;
  }
  public function setShowAll( $b ) {
    $this->showall = $b;
  }

  /**
   *  @returns how to render the evaluation result ( true = table-form; false = twig)
   *  @param isemail - if true it will return the email settings for rendering way
   */
  public function isRenderAsForm( $isemail = false ) {
    if ($isemail)
      return $this->emailrenderasform;

    return $this->renderasform;
  }
  public function setRenderAsForm( $b, $isemail = false ) {
    if ($isemail)
      $this->emailrenderasform = $b;
    else
      $this->renderasform = $b;
  }

  public function isIncludePTitle( $isemail = false ) {
    if ($isemail)
      return $this->includetitleinemail;

    return $this->includetitle;
  }
  public function setIncludePTitle( $b, $isemail = false ) {
    if ($isemail)
      $this->includetitleinemail = $b;
    else
      $this->includetitle = $b;
  }

  public function isSendInEmail() {
    return $this->sendinemail;
  }
  public function setSendInEmail( $b ) {
    $this->sendinemail = $b;
  }

  public function getCollectorEmailAddress() {
    return $this->email;
  }
  public function setCollectorEmailAddress( $email ) {
    $this->email = $email;
  }

  public function isSendToUser() {
    return $this->sendtouser;
  }
  public function setSendToUser( $b ) {
    $this->sendtouser = $b;
  }

  /**
   *  @returns array of element(s) data
   *  @param element - the selected element id
   */
  public function getElementsData( $element = null ) {
    return unserialize($this->elements_data);
  }
  public function setElementsData( $elements ) {
    if (is_array($elements))
      $this->elements_data = serialize($elements);
    else
      $this->elements_data = $elements;
  }

  public function getConditionsData() {
    return $this->conditions_data;
  }
  public function setConditionsData( $conditions ) {
    if (is_array($conditions))       //may check isset($conditions['table']);
      $this->conditions_data = serialize($conditions);
    else
      $this->conditions_data =  $conditions;
  }

   /**
   *    Finds and return the evalwf entity which were saved with selected webform
   *    @param string wfid - the selected webform_id
   *    @returns evlwf entity
   */
  static public function LoadEntityByWebformID( $wfid ) {
    $query = \Drupal::entityQuery('evalwf')->accessCheck(TRUE)->condition('webform_id', $wfid);
    $entities = $query->execute();
    if ( !empty($entities) )
      $eid = reset($entities);

    if(isset($eid)) $entity = \Drupal::entityTypeManager()->getStorage('evalwf')->load($eid);
    return ( isset($entity) ? $entity : null );
  }

   /**
   *    Recursively walk through webform elements and creates a list from them
   *    @param array webform_elements   - the selected webform elements array
   *    @returns array - the list of elements key and title
   */
  function getelementslist( $webform_elements ) {
    $elements_list = [];
    foreach ( $webform_elements as $k => $i) {
      if ( ($i) && isset($i['#type']) ) {
        switch( $i['#type'] ) {
          case 'fieldset':
          case 'container':
          case 'details':
            $elements_list = array_merge( $elements_list, array($k => (isset($i['#title']) ? $i['#title'] : $k )) );
            $elements_list = array_merge( $elements_list, $this->getelementslist($i) );
            break;
          case 'checkboxes':
          case 'radios':
          case 'select':
          case 'tableselect':
            if (isset($i['#title'])) {
              $elements_list = array_merge( $elements_list, array($k => $i['#title']) );
            }
            break;
          default:
            break;
        }
      }
    }
    return $elements_list;
  }

   /**
   *    My ajax callback for any button or other elements
   *    @param array form                    - the current form
   *    @param FormStateInterface form_state - the current FormStateInterface
   *    @returns array - the modified form elements
   */
  public function myAjaxCallback(array &$form, FormStateInterface $form_state){
    unset($form['conditions'][0]['table']['#prefix']);
    unset($form['conditions'][0]['table']['#suffix']);

    // \Drupal::messenger()->deleteAll();

    return $form['conditions'][0]['table'];
  }

  /**
   *    Recursively find the item in imput elements and returns points array
   *    @param array  i     - the input elements array
   *    @param string key   - the items key
   *    @returns points array
   */
  function mygetValue($i, $key) {
      $value = null;
      foreach ( $i as $k => $v ) {
        if ($k===$key) { $value=$v;}
        else {
          if (is_array($v)) {
            if ($value==null) { $value = $this->mygetValue($v, $key); }
          }
        }
      }
      return $value;
    }

  function PointsInputValidate( $element, $form_state) {
    $i = $form_state->getUserInput()['conditions'][0];
    $v = $this->mygetValue( $i['table'] , $element['#id'] );
    if ( !is_numeric($v[$element['#parents'][4]]) ) {
      $form_state->seterror($element, t('You must enter valid numbers.'));
    }
  }

  function URLValidate( $element, $form_state) {
    $i = $form_state->getUserInput()['conditions'][0];
    $v = $this->mygetValue( $i['table'] , $element['#id'] );
    $url = ($element['#id']==='elsecondition' ? (isset($v['elseurl']) ? $v['elseurl'] : null): (isset($v['url']) ? $v['url'] : null) );
    if ( !in_array( explode('://', trim($url) )[0],['http','https']) ) {
      if ( !$url || !in_array( explode('/', trim($url) )[0],['internal:','']) ) {
        $form_state->seterror($element, t('You must enter valid URLs. Please start with http(s):// or internal:/ or just /.'));
      }
    }
  }

   /**
   *    This creates a settings table from conditions
   *    @param FormStateInterface form_state - the current FormStateInterface
   *    @param array op                      - operation to do (add,delete,move up)
   *    @returns array - the (modified) conditions settings table
   */
  public function CreateConditionsTable( FormStateInterface $form_state, $op ) {
    $webform = Webform::load($this->getWebformId());
    $webform_elements = $webform->getElementsDecoded();
    $e = $this->getelementslist($webform_elements);
    $e = array_merge( array( '' => '' , 'AllTotal' => t('Total points') ), $e );

    $data = $form_state->getUserInput();
    if ($data && isset($data['conditions'][0])) {
      $this->setConditionsData( serialize( $data['conditions'][0] ) );
    }

    $unserializedconditions = !empty($this->getConditionsData()) ? unserialize($this->getConditionsData()) : null;
    $conditions = isset($unserializedconditions['table']) ? $unserializedconditions['table'] : null;

    $conditionsform['table'] = array(
      '#type' => 'table',
      '#id' => 'table',
      '#caption' => t('Conditions').':',
      '#header' => array( '', t('Total of'),'',t('lo'),'',t('hi'),'',t('Url to jump'),'' ),
    );

    $index = 0;
    if ($conditions) {
      foreach ($conditions as $ckey => $condition) {
        if (is_numeric($ckey)) {
          if ( ($op) && ($op['op']=='delrow') && ($op['value']==$index) ) {
            $op['op'] = 'Deleted';
          }
          else {
            $condi = array(
              array( '#type' => 'markup', '#markup' => t('if') ),
              'telem' => array(
                '#type' => 'select',
                '#options' => $e,
                '#value' => $condition['telem'],
              ),
              array( '#type' => 'markup', '#markup' => t('beetween (') ),
              'lo' => array(
                '#type' => 'textfield',
                '#id' => $index,
                '#size' => 3,
                '#value' => $condition['lo'],
                '#element_validate' => [ [ $this, 'PointsInputValidate' ] ],
              ),
              array( '#type' => 'markup', '#markup' => ',' ),
              'hi' => array(
                '#type' => 'textfield',
                '#id' => $index,
                '#size' => 3,
                '#value' => $condition['hi'],
                '#element_validate' => [ [ $this, 'PointsInputValidate' ] ],
              ),
              array( '#type' => 'markup', '#markup' => t(') then') ),
              'url' => array(
                '#type' => 'textfield',
                '#id' => $index,
                '#size' => 40,
                '#value' => $condition['url'],
                '#element_validate' => [ [ $this, 'URLValidate' ] ],
              ),
              'buttons' => [
                ( $index==0 ? [] : array(
                  '#type' => 'button',
                  '#id' => 'Up-'.$index,
                  '#name' => 'Up-'.$index,
                  '#value' => t('Up'),
                  '#row' => $index,
                  '#ajax' => [
                    'callback' => [$this, 'myAjaxCallback'],
                    'disable-refocus' => TRUE,
                    'event' => 'click',
                    'wrapper' => 'table',
                  ],
                ) ),
                array(
                  '#type' => 'button',
                  '#id' => 'Delete-'.$index,
                  '#name' => 'Delete-'.$index,
                  '#value' => t('Del'),
                  '#row' => $index,
                  '#ajax' => [
                    'callback' => [$this, 'myAjaxCallback'],
                    'disable-refocus' => TRUE,
                    'event' => 'click',
                    'wrapper' => 'table',
                  ],
                ),
              ],
            );
            if ( ($op) && ($op['op']=='uprow') && ($op['value']==$index) ) {
              $r = $conditionsform['table'][$index-1];
              $b = $condi['buttons'];
              $condi['buttons'] = $r['buttons'];
              $r['buttons'] = $b;
              $condi['lo']['#id']=$index-1;
              $condi['hi']['#id']=$index-1;
              $condi['url']['#id']=$index-1;
              $r['lo']['#id']=$index;
              $r['hi']['#id']=$index;
              $r['url']['#id']=$index;
              $conditionsform['table'][$index-1] = $condi;
              $conditionsform['table'][$index++] = $r;
            }
            else {
              $conditionsform['table'][$index++] = $condi;
            }
          }
        }
      }
    }

    if ( ($op) && ($op['op']=='addrow')) {
      for ( $i=0; $i<$op['value']; $i++ ) {
        $condi = array(
          array( '#type' => 'markup', '#markup' => t('if') ),
          'telem' => array(
            '#type' => 'select',
            '#options' => $e,
          ),
          array( '#type' => 'markup', '#markup' => t('beetween (') ),
          'lo' => array(
            '#type' => 'textfield',
            '#id' => $index,
            '#size' => 3,
            '#element_validate' => [ [ $this, 'PointsInputValidate' ] ],
          ),
          array( '#type' => 'markup', '#markup' => ',' ),
          'hi' => array(
            '#type' => 'textfield',
            '#id' => $index,
            '#size' => 3,
            '#element_validate' => [ [ $this, 'PointsInputValidate' ] ],
          ),
          array( '#type' => 'markup', '#markup' => t(') then') ),
          'url' => array(
            '#type' => 'textfield',
            '#id' => $index,
            '#size' => 40,
            '#element_validate' => [ [ $this, 'URLValidate' ] ],
          ),
          'buttons' => [
            ( $index==0 ? [] : array(
              '#type' => 'button',
              '#id' => 'Up-'.$index,
              '#name' => 'Up-'.$index,
              '#value' => t('Up'),
              '#row' => $index,
              '#ajax' => [
                'callback' => [$this, 'myAjaxCallback'],
                'disable-refocus' => TRUE,
                'event' => 'click',
                'wrapper' => 'table',
              ],
            ) ),
            array(
              '#type' => 'button',
              '#id' => 'Delete-'.$index,
              '#name' => 'Delete-'.$index,
              '#value' => t('Del'),
              '#row' => $index,
              '#ajax' => [
                'callback' => [$this, 'myAjaxCallback'],
                'disable-refocus' => TRUE,
                'event' => 'click',
                'wrapper' => 'table',
              ],
            ),
          ],
        );

        $conditionsform['table'][$index++] = $condi;
      }
    }

    $conditionsform['table']['elsecondition'] = array(
      'text' => [
        '#type'=> 'markup',
        '#markup' => t('Else (if none of above true):'),
        '#wrapper_attributes' => [ 'colspan' => 7 ],
      ],
      'elseurl' => array(
        '#type' => 'textfield',
        '#id' => 'elsecondition',
        '#value' => (
          isset($conditions['elsecondition']) && isset($conditions['elsecondition']['elseurl'])
          ? $conditions['elsecondition']['elseurl'] : ''
        ),
        '#size' => 40,
        '#disabled' => (
          isset($conditions['elsecondition']) &&
            isset($conditions['elsecondition']['iselsecondition']) &&
            ($conditions['elsecondition']['iselsecondition'])
          ? FALSE : TRUE
        ),
      ),
      'iselsecondition' => [
        '#type' => 'checkbox',
        '#title' => t('Enable'),
        '#value' => (
          isset($conditions['elsecondition']) &&
            isset($conditions['elsecondition']['iselsecondition']) &&
            ($conditions['elsecondition']['iselsecondition'])
          ? TRUE : FALSE
        ),
        '#ajax' => [
          'callback' => [$this, 'myAjaxCallback'],
          'disable-refocus' => TRUE,
          'event' => 'change',
          'wrapper' => 'table',
        ],
      ],
    );
    if (!$conditionsform['table']['elsecondition']['elseurl']['#disabled']) {
      $conditionsform['table']['elsecondition']['elseurl']['#element_validate'] = [ [ $this, 'URLValidate' ] ];
    }
    return $conditionsform;
  }

  /**
   *   delete EvalWFEvaluation entities connected to selected EvalWF evaluation settings entity
   */
  public function delete() {
    $wfid = $this->getWebformId();
    $query = \Drupal::entityQuery('evalwf_evaluation')->accessCheck(FALSE)->condition('wfid', $wfid);
    $res = $query->execute();
    foreach ($res as $eid) {
      if(!empty($eid)) {
        $evaluation = \Drupal::entityTypeManager()->getStorage('evalwf_evaluation')->load($eid);
        $evaluation->delete();
      }
    }
    parent::delete();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    parent::getCacheTags();
    $cache_tags[] = 'evalwf:' . $this->getId();
    $cache_tags[] = 'config:evalwf.evalwf.'. $this->getId();
    $cache_tags[] = 'config:evalwf_list';
//    $cache_tags[] = 'route_match:evalwf.withpath.content';
//    $cache_tags[] = 'config:evalwf.settings';
    return $cache_tags;
  }

  /**
   *    Saves entity
   */
  public function save() {
    parent::save();
    Cache::invalidateTags($this->getCacheTags());
  }

}
?>
