<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Access\EvalWFEvaluationAccess.php - The Evaluation Entity Access for Controller:content
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the custom access control handler for the user accounts.
 */
class EvalWFEvaluationAccess {

  public static function checkAccess(AccountInterface $account) {
    $wfs = \Drupal::routeMatch()->getParameter('webform_submission');
    if (empty($wfs)) {
      $urlquery = \Drupal::request()->query->all();
      if (isset($urlquery['sid']))
        $sid=$urlquery['sid'];
    }
    else
      $sid = $wfs->id();

    if (!empty($sid) && $account->haspermission('access evalwf evaluations')) {
      $query = \Drupal::entityQuery('evalwf_evaluation')->accessCheck(FALSE)->condition('sid', $sid);
      $res = $query->execute();
      if (!empty($res)) { // if evaluated
        if ($account->id()==0) {
          return AccessResult::forbidden();
        }
      }

      $wfs = \Drupal::entityTypeManager()->getStorage('webform_submission')->load($sid);
      if (empty($wfs) || $wfs->getOwnerId()!=$account->id())
        return AccessResult::forbidden();

      return AccessResult::allowed();
    }
    // path: '/evalwf' also allowed
    return AccessResult::allowed();
  }

}
