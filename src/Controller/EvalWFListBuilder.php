<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Controller\EvalWFListBuilder.php - Creates EvalWF settings list
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Controller;

use Drupal\Core\Url;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\evalwf\Helper\EvalWFTxtHelper;

/**
*  Class of ListBuilder for EvalWF settings
*/
class EvalWFListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name of Evaluation');
    $header['webform_id'] = $this->t('Webform ID');
    $header['showevalpage'] = $this->t('Display Evaluation');
    $header['showall'] = $this->t('Display all');
    $header['sendinemail'] = $this->t('Send');
    $header['email'] = $this->t('Email address');
    $header['submissions'] = $this->t('Submissions');
    $header['evaluations'] = $this->t('Evaluations');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['webform_id'] = $entity->getWebformId();
    $row['showevalpage'] = EvalWFTxtHelper::BoolToTxt( $entity->isShowEvaluationPage() );
    $row['showall'] = EvalWFTxtHelper::BoolToTxt( $entity->isShowAll() );
    $row['sendinemail'] = EvalWFTxtHelper::BoolToTxt( $entity->isSendInEmail() );
    $row['email'] = $entity->getCollectorEmailAddress();

    $query = \Drupal::entityQuery('webform_submission')->accessCheck(FALSE)->condition('webform_id', $entity->getWebformId());
    $res = $query->execute();
    $row['submissions'] = ($res ? count($res) : '');;

    $query = \Drupal::entityQuery('evalwf_evaluation')->accessCheck(FALSE)->condition('wfid', $entity->getWebformId());
    $res = $query->execute();
    $row['evaluations'] = ($res ? array (
        'data' => array(
          '#type' => 'link',
          '#title' => count($res),
          '#url' => \Drupal\Core\Url::fromRoute('entity.evalwf_evaluation.collection'),
        )
      )
      : '');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if (\Drupal::currentUser()->hasPermission('administer evalwf settings')) {
      $operations['edit'] = array(
        'title' => t('Edit Settings'),
        'url' => Url::fromRoute('entity.evalwf.withpath.settings_form', ['webform' => $entity->getWebformId()] ),
        'weight' => 40,
      );
      $operations['add'] = array(
        'title' => t('Add Evaluation'),
        'url' => Url::fromRoute('entity.evalwf.add_form'),
        'weight' => 50,
      );
    }
    $operations['export'] = array(
      'title' => t('Export Settings'),
      'url' => Url::fromRoute('config.export_single', ['config_type' => 'evalwf', 'config_name' => $entity->getId() ] ),
      'weight' => 60,
    );
    return $operations;
  }

}

?>
