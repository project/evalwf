<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Controller\EvalWFPluginController.php - Creates admin side content / the Administer EvalWF Evaluation module plugin settings page
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Controller;

//use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

class EvalWFPluginController extends ConfigFormBase { //ControllerBase {

  protected $pluginManager;

  public static function create(ContainerInterface $container) {
    $e = parent::create($container);
    $e->pluginManager = $container->get('plugin.manager.evalwfelement');
    return $e;
  }

  /**
   * Returns the evaluation page title. / HTML <title> - Drupal will append sitename/
   */
  public function getTitle() {
    return $this->t('Administer EvalWF Evaluation module plugin settings');
  }

  public function getFormId() {
    return 'evalwf.plugin_configuration_form';
  }

  protected function getEditableConfigNames() {
    $names = [
      'pluginsettings',
    ];
    return $names;
  }

  /**
   *   Creates content form - the Administer EvalWF Evaluation module plugin settings page
   *   @returns form array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = \Drupal::configFactory()->getEditable('evalwf.settings');
    $pluginsettings = unserialize($config->get('pluginsettings'));

    $res = [];
    $definitions = $this->pluginManager->getSortedDefinitions();
    foreach (array_keys($definitions) as $key) {
      foreach ( $definitions[$key]['types'] as $type ) {
        if (!isset($res[$type])) {
          $res[$type] = [];
        }
        $res[$type][] = $definitions[$key];
      }
    }

    $rows = [];
    foreach ( $res as $k => $r ) {
      foreach ( $r as $item ) {
        $rows[$k][] = [
          'key' => $k,
          'select' => [],
          'id' => $item['id'],
          'label' => $item['label'],
          'class' => $item['class'],
          'provider' => $item['provider'],
        ];
      }
    }

    $form['pluginlist'] = [
      '#type' => 'table',
      '#caption' => $this->t( 'List of installed EvalWFElement plugins:' ),
      '#header' => [
        $this->t('element type'),
        $this->t('prefered'),
        $this->t('id'),
        $this->t('label'),
        $this->t('object class'),
        $this->t('provider module'),
      ],
      '#suffix' => '<br><br>',
      '#rows' => [],
    ];
    foreach ($rows as $key => $items) {
      $c = 0;
      foreach ($items as $item) {
        if ( count($items)>1 ) {
          $item['select'] = [
            'data' => array (
              '#type' => 'radio',
              '#name' => 'evalwfelement_'.$item['key'],
              '#id' => $item['id'],
              '#return_value' => $item['id'],
              '#value' => (( isset($pluginsettings[$item['key']]) && (strcmp($pluginsettings[$item['key']],$item['id'])==0) ) ? $item['id']: null),
            ),
          ];
          if ($c==0) {
            $item['key'] = [
              'data' => $item['key'],
              'rowspan' => '2',
            ];
          }
          else {
            unset($item['key']);
          }
        }
        else {
          $item['select'] = '';
        }
        $form['pluginlist']['#rows'][] = $item;
        $c++;
      }
    }
//    \Drupal::messenger()->addMessage( serialize($pluginsettings) , 'warning');
//    \Drupal::messenger()->addMessage( serialize($rows) , 'warning');

    $form = parent::buildForm($form,$form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tags[] = 'config:evalwf.settings';
    return $cache_tags;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();

    $definitions = $this->pluginManager->getDefinitions();

    $settings = [];
    $elementkeys = [];
    foreach (array_keys($definitions) as $key)
      foreach ($definitions[$key]['types'] as $type)
        $elementkeys[$type] = $type;

    foreach ($elementkeys as $key => $value)
      if (isset($input['evalwfelement_'.$key]))
        $settings[$key]=$input['evalwfelement_'.$key];

    // save:
    $config = \Drupal::configFactory()->getEditable('evalwf.settings');
    $config->set('pluginsettings', serialize($settings));
    $config->save();

    //onsucess
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));

    Cache::invalidateTags($this->getCacheTags());
  }

}
?>
