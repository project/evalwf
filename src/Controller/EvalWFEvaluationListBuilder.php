<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Controller\EvalWFEvaluationListBuilder.php - Creates EvalWF settings list
*
* @author Tóthpál István
*
* @references: WebformEntityListBuilder
*              Tablesort and translated headers: https://www.drupal.org/project/drupal/issues/2935344
*/

namespace Drupal\evalwf\Controller;

use Drupal\Core\Url;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\wevform\WebformSubmissionListBuilder;
use Drupal\evalwf\Helper\EvalWFTxtHelper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
*  Class of ListBuilder for EvalWF settings
*/
class EvalWFEvaluationListBuilder extends ConfigEntityListBuilder {

  public $filter = null;

  public $request;

  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['ID'] = [
      'data' => $this->t('ID'),
      'specifier' => 'id',
      'field' => 'id',
    ];
    $header['Webform'] = [
      'data' =>  $this->t('Webform'),
      'specifier' => 'wfid',
      'field' => 'wfid',
    ];
    $header['Submission'] = [
      'data' => $this->t('Submission'),
      'specifier' => 'sid',
      'field' => 'sid',
    ];
    $header['Total points'] = [
      'data' => $this->t('Total points'),
      'specifier' => 'total_points',
      'field' => 'total_points',
    ];
    $header['Sent'] =  [
      'data' => $this->t('Sent'),
      'specifier' => 'sent',
      'field' => 'sent',
    ];
    $header['Email'] = [
      'data' => $this->t('Email'),
      'specifier' => 'sentto',
      'field' => 'sentto',
    ];
    $header['Timestamp'] = [
      'data' => $this->t('Timestamp'),
      'specifier' => 'timestamp',
      'field' => 'timestamp',
      'sort' => 'desc',
    ];
    $header['UserID'] =  [
      'data' => $this->t('UserID'),
      'specifier' => 'uid',
      'field' => 'uid',
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if ( $this->filter==null  || ( $this->filter!=null && $entity->getWfId()==$this->filter)) {
      $row['ID'] = $entity->getId();
      $row['Webform'] = $entity->getWfId();
      $row['Submission'] = $entity->getSid();
      $row['Total points'] = $entity->getTotal_Points();
      $row['Sent'] = EvalWFTxtHelper::BoolToTxt( $entity->isSent() );
      $row['Sentt o'] = $entity->getSentTo();
      $row['Timestamp'] = $entity->getTimestamp();
      $row['UserID'] = $entity->getUserName();
      return $row + parent::buildRow($entity);
    }
    else {
      return null;
    }
  }

  public function renderfilter() {
    $this->filter = $this->request->query->get('filter');

    $options[null]=null;
    $query = \Drupal::entityQuery('webform')->accessCheck(FALSE);
    $wfids = $query->execute();
    if ( !empty($wfids) )
      $options = array_merge( $options, $wfids );

    $form['filter_table'] = [
      '#type' => 'table',
        0 => [
        'filter' => array(
          '#type' => 'select',
          '#title' => t('Select webform'),
          '#options' => $options,
          '#value' => $this->filter,
        ),
        'apply' => [
          '#type' => 'submit',
          '#value' => t('Apply filter'),
        ],
      ],
    ];

    $form = array_merge( $form, $this->render() );

    return $form;
  }

  public function render() {
    $form[] = parent::render();
    return $form;
  }

  public function getEntityIds() {
    $header = $this->buildHeader();
    $order = $this->request->query->get('order');
    $sort = $this->request->query->get('sort');

    if ( !empty($order))
      foreach ( $header as $row ) {
        if (is_array($row))
          if ( (string)($row['data']->render()) == $this->t($order) )
            $torder = $row['data']->getUntranslatedString();
      }
    $query = $this->getStorage()->getQuery()->sort( $order ? $header[$torder]['field'] : 'timestamp', $sort ? $sort : 'desc' );
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if (\Drupal::currentUser()->hasPermission('administer evalwf settings') || (\Drupal::currentUser()->id()==$entity->getUserId())) {
      $operations['view'] = array(
        'title' => t('View'),
        'url' => Url::fromRoute('entity.evalwf_evaluation.view_form', ['evalwf_evaluation' => $entity->getId()] ),
        'weight' => 10,
      );
    }
/*    if (\Drupal::currentUser()->hasPermission('delete evalwf evaluation') || (\Drupal::currentUser()->id()==$entity->getUserId())) {
      $operations['delete'] = array(
        'title' => t('Delete'),
        'url' => Url::fromRoute('entity.evalwf_evaluation.delete_form', [
          //'evalwf' => $entity->getWfId(),
          'evalwf_evaluation' => $entity->getId()
        ]),
        'weight' => 100,
      );
    }
*/
    return $operations;
  }

}

?>
