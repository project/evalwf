<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Controller\EvalWFController.php - Creates user side content / the evaluation page
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Mail\Plugin\Mail\PhpMail;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\evalwf\Entity\EvalWF;
use Drupal\evalwf\Entity\EvalWFEvaluation;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EvalWFController extends ControllerBase {

  private $entity = null;
  private $evalwf_id = null;
  private $evaluation_id = null;

  protected $pluginManager;

  public static function create(ContainerInterface $container) {
    $e = parent::create($container);
    $e->pluginManager = $container->get('plugin.manager.evalwfelement');
    return $e;
  }

  public function __construct() {
    $this->pluginManager = \Drupal::getContainer()->get('plugin.manager.evalwfelement');
  }

  public function getCacheTags() {
    $cache_tags[] = 'config:evalwf.settings';
    $cache_tags[] = 'config:evalwf.evalwf_config.evalwf_config';
    $cache_tags[] = 'config:evalwf.evalwf.'. $this->getEvalWFId();
    $cache_tags[] = 'config:evalwf.evalwf_evaluation.'. $this->getEvaluationId();
    $cache_tags[] = 'config:webform.webform.'. $this->getEvalWFId();
    $cache_tags[] = 'webform_submission:'. $this->getEvaluationId();
    return $cache_tags;
  }

  protected function getEvalWFId() {
    return $this->evalwf_id;
  }
  protected function setEvalWFId( $id ) {
    $this->evalwf_id = $id;
  }

  protected function getEvaluationId() {
    return $this->evaluation_id;
  }
  protected function setEvaluationId( $id ) {
    $this->evaluation_id = $id;
  }

  /**
   *   Loads the selected Entity
   *
   *     - @params comes from route: evalwf/[webform:id]/[webform_submission:sid]
   *     or from urlquery: evalwf?wfid=[webform:id]&sid=[webform_submission:sid]
   *
   *   @param Webform weform                         - the selected webform
   *   @param WebformSubmission webform_submission   - the selected webform_submission
   *
   *   this->entity    - the loaded entity
   *   this->wfid      - the selected webform_id
   *   this->sid       - the selected webform_submission_id
   *   this->webform_elements    - the form elements in the selected webform
   *   this->webform_submission  - the selected webform_submission
   */
  function LoadEntity( Webform $webform = null, WebformSubmission $webform_submission = null) {
    if ($webform && $webform_submission) {
      if ($webform->id() == $webform_submission->bundle()) {
        $this->wfid = $webform->id();
      }
      else {
        $this->wfid = $webform_submission->bundle();
        $webform = Webform::load($this->wfid);
        \Drupal::messenger()->addMessage( t('Different webform connected to selected submission, evaluated with submission\'s webform.'), 'warning');
      }
      $this->sid = $webform_submission->id();
    }
    else {
      $urlquery = \Drupal::request()->query->all();
      if (isset($urlquery['sid'])) $this->sid=$urlquery['sid'];
      if (isset($urlquery['wfid'])) $this->wfid=$urlquery['wfid'];
    }

    if (isset($this->sid) && isset($this->wfid)) {
      if(!$webform) $webform = Webform::load($this->wfid);
      $this->webform_elements = $webform->getElementsDecoded();
      $this->webform_submission = WebformSubmission::load($this->sid);
    }
    if (isset($this->wfid)) {
      $this->entity = EvalWF::LoadEntityByWebformID($this->wfid);
    }
  }

  /**
   * Returns the evaluation page title. / HTML <title> - Drupal will append sitename/
   */
  public function getTitle( Webform $webform = null, WebformSubmission $webform_submission = null) {
    $this->LoadEntity($webform, $webform_submission);
    if ($this->entity) {
      return  $this->entity->getEvaluationTitle();
    }
  }

  /**
   *    Creates the elements tree for theme
   *
   *   @param array  elements   - webform elements
   *   @param array  data       - webform submission data
   *   @param array  settings   - the evaluation settings data
   *   @returns array
   *        array  childs           -  elements theme data
   *        string subtotal         -  total points of all elements
   *        array  elements_points  -  elements and its evaluated points
   */
  private function buildelementstree( $elements, $data, $settings ) {
    $tree = [];
    $points = 0;
    $elements_points = [];
    foreach ($elements as $key => $element ) {
      $subpoints = 0;
      if (isset($element['#type'])) {
        $tree[$key]['type'] = $element['#type'];
        $tree[$key]['title'] = ( isset($element['#title']) ? $element['#title'] : ( isset($element['#caption']) ? $element['#caption'] : '' ));
        $tree[$key]['description'] = ( isset($element['#description']) ? $element['#description'] : '' );
        $tree[$key]['header'] = [ ['data'=>'', 'attributes'=>' width=5'],t('answer'),t('points'), ];
        switch ($element['#type']) {
          default:
            $plugin = $this->pluginManager->getPluginObjectFor($element['#type']);
            if (!empty($plugin)) {
              $builtdata = $plugin->buildResultTwigData( $element, $data, $settings, $key, 0, $this->entity->isShowAll() );
              if ( !empty($builtdata['tree']) )
                $tree[$key] = array_merge( $tree[$key], $builtdata['tree'][$key] );

              if ( !empty($builtdata['subpoints']) )
                $subpoints = $builtdata['subpoints'][$key];

              if( isset($builtdata['elements_points']) )
                $elements_points = array_merge( $elements_points, $builtdata['elements_points'] );
              else
                $elements_points = array_merge( $elements_points, array( $key => $subpoints ) );
            }
            else {
              $tree[$key] = [];
            }
            break;
        }
        $points += $subpoints;
      }
    }
    return [
      'childs' => $tree,
      'subtotal' => $points,
      'elements_points' => $elements_points,
    ];
  }

  /**
   *   Creates content form - the evaluation page
   *
   *     - @params comes from route: evalwf/[webform:id]/[webform_submission:sid]
   *     or from urlquery: evalwf?wfid=[webform:id]&sid=[webform_submission:sid]
   *
   *   @param Webform weform                         - the selected webform
   *   @param WebformSubmission webform_submission   - the selected webform_submission
   *   @param boolean isadmin                        - true if admin side called this
   *   @returns form array
   */
  public function content( Webform $webform = null, WebformSubmission $webform_submission = null, $isadmin = false ) {
    $form = $this->LoadEntity($webform, $webform_submission);
    $query = \Drupal::entityQuery('evalwf_config')->accessCheck(TRUE)->condition('id', 'evalwf_config');
    $result = $query->execute();
    if ($result) $this->entityconfig = \Drupal::entityTypeManager()->getStorage('evalwf_config')->load('evalwf_config');

    if ($this->entity) {
      $form['title']=array(
        '#type' => 'markup',
        '#markup' => '<h1>'. $this->entity->getEvaluationTitle() .'</h1>',
      );
      $form[]=array(
        '#type' => 'markup',
        '#markup' => $this->entity->getEvaluationIntro(),
        '#suffix' => '<br><br>',
      );

      if (!empty($this->sid) && !empty($this->wfid) && !empty($this->entity->getElementsData())) {
        $result = $this->getElementsForm($this->webform_elements, $this->webform_submission->getData(), $this->entity->getElementsData());
        $elements_form[] = $result['form'];
        $elements_form[] = array(
          '#type' => 'markup',
          '#markup' => '<h3><b><u>'.t('Total: ').$result['points'].' points</u></b></h3>',
        );
        $conditions_form = $this->getConditionsForm( array_merge( array('AllTotal' => $result['points']), $result['elements_points']) );
        $conditions = $this->getConditionsData( array_merge( array('AllTotal' => $result['points']), $result['elements_points']));
        if ($conditions) {
          $conditions[0]['title'] = t('Continue').' >>>';
          $conditions[0]['url'] = Url::fromUri($conditions[0]['url'])->setAbsolute(TRUE)->toString();
        }

        $elements = $this->buildelementstree(
          $this->webform_elements,
          $this->webform_submission->getData(),
          $this->entity->getElementsData()
        );
        $elements['childs']['AllTotal'] = [
          'title' => t('Your final result: %subtotal points', [ '%subtotal' => $elements['subtotal']]),
        ];
        $build = [
          '#theme' => 'evalwf_theme_hook',
          '#settings' => [
            'title' => $this->entity->getEvaluationTitle(),
            'html' => $this->entity->getEvaluationIntro(),
          ],
          '#elements' => $elements['childs'],
          '#conditions' => $conditions,
          '#attached' => [
            'library' => [
              'evalwf/evalwf-resultpage',
            ],
          ],
        ];

        $query = \Drupal::entityQuery('evalwf_evaluation')->accessCheck(FALSE)->condition('id', $this->sid);
        $res = $query->execute();
        if ($res) {
          $evaluation = \Drupal::entityTypeManager()->getStorage('evalwf_evaluation')->load($this->sid);
          $checksum = $evaluation->getCheckSum();
          if (md5(serialize($this->webform_elements))!=$checksum['webform']) {
            \Drupal::messenger()->addMessage( t('The webform elements has been modified, this result may different than the previously saved evaluation.'),'warning');
          }
          if (md5(serialize($this->webform_submission->getData()))!=$checksum['submission']) {
            \Drupal::messenger()->addMessage( t('The webform submission has been modified, this result may different than the previously saved evaluation.'),'warning');
          }
          if (md5(serialize($this->entity->getElementsData()) . $this->entity->getConditionsData())!=$checksum['evalwf_setting']) {
            \Drupal::messenger()->addMessage( t('The evaluation settings has been changed, this result may different than the previously saved evaluation.'),'warning');
          }
        }
        else {
          $evaluation = new EvalWFEvaluation(
            [
              'id' => $this->sid,
              'wfid' => $this->wfid,
              'sid' => $this->sid,
              'total_points' => $result['points'],
              'elements_points' => serialize($result['elements_points']),
              'conditions_data' =>
                serialize($this->getConditionsData( array_merge( array('AllTotal' => $result['points']), $result['elements_points']) )),
              'sent' => false,
              'timestamp' => date('Y-m-d H:i:s', time()),
              'uid' => ( isset($this->entityconfig) && $this->entityconfig->isEvalAsUser() ?
                  $this->webform_submission->getOwner()->id() :
                  \Drupal::currentUser()->id()
                ),
              'checksum' => serialize(
                array(
                  'webform' => md5(serialize($this->webform_elements)),
                  'submission' => md5(serialize($this->webform_submission->getData())),
                  'evalwf_setting' => md5(serialize($this->entity->getElementsData()) . $this->entity->getConditionsData()),
                  'evalwf_evaluation' => md5(serialize($result['elements_points']) . serialize($conditions_form)),
                )
              ),
            ],
            'evalwf_evaluation'
          );
        }

        if ((($this->entity->isSendInEmail() || $this->entity->isSendToUser()) && !$evaluation->isSent()) &&
            ( ($isadmin && isset($this->entityconfig) && $this->entityconfig->isAdminSendMail()) || (!$isadmin) ) ){

          if ( $this->entity->isRenderAsForm(true) ) {
            if (!$this->entity->isIncludePTitle(true))
              unset($form['title']);
            $body = array_merge( $form, $elements_form );
            if (isset($conditions_form[0])) {
              $conditions_form[0]['#url']->setAbsolute(TRUE);
              $body = array_merge( $body, $conditions_form );
            }
          }
          else {
            if (!$this->entity->isIncludePTitle(true))
              unset( $build['#settings']['title'] );
            $body = $build;
          }
          $this->setEvalWFId($this->wfid);
          $this->setEvaluationId($this->sid);
          $body['#cache']['tags'] = $this->getCacheTags();

          $submitter = ( $isadmin ? $this->webform_submission->getOwnerId() : \Drupal::currentUser()->id() );
          $sendto = '';
          $sendto .= ($this->entity->isSendInEmail() ? $this->entity->getCollectorEmailAddress() : '' );
          $useraddr = '';
          $useraddr .= ($this->entity->isSendToUser() ? \Drupal\user\Entity\User::load( $submitter )->getEmail() : '');
          $sendto .= (($sendto=='' || $useraddr=='') ? '' :  ',') . $useraddr;

         if ( $sendto && $body ) {
            if ( isset($this->entityconfig) && $this->entityconfig->getSubject() ) {
              $subject = (new FormattableMarkup(
                  $this->entityconfig->getSubject(),
                  [ '@wfid' => $this->wfid, '@sid' => $this->sid, ]
                ))->__toString();
            }
            else {
              $subject = t( 'EvalWF - Evaluation of submission @wfid-@sid', [ '@wfid' => $this->wfid, '@sid' => $this->sid, ]);
            }
            $this->send_mail(
              ( isset($this->entityconfig) && $this->entityconfig->getSenderName() ?
                  $this->entityconfig->getSenderName() :
                  t('EvalWF at @site', [ '@site' => \Drupal::config('system.site')->get('name')])
              ),
              ( isset($this->entityconfig) && $this->entityconfig->getSender() ?
                  $this->entityconfig->getSender() :
                  \Drupal::config('system.site')->get('mail')
              ),
              $sendto,
              $subject,
              \Drupal::service('renderer')->render($body)->__toString(),
              ( isset($this->entityconfig) ?
                  $this->entityconfig->getMailer() :
                  'drupalmail'
              )
            );
          $evaluation->setSent( true );
          $evaluation->setSentTo( $sendto );
          }
        }
        $evaluation->save();
        if ($this->entity->isShowEvaluationPage()) {
          $form = array_merge( $form, $elements_form );
        }
        if ($conditions_form) $form = array_merge( $form, $conditions_form );
        if (!$this->entity->isIncludePTitle()) {
          unset($form['title']);
        }
        $this->setEvalWFId($this->wfid);
        $this->setEvaluationId($this->sid);
        $form['#cache']['tags'] = $this->getCacheTags();
      }
      else {
        if (isset($this->sid) && isset($this->wfid) && empty($this->entity->getElementsData())) {
          $form[]=array(
            '#type' => 'markup',
            '#markup' => t('Evaluation data currenly not available, please come back later...').'<br>',
          );
          if ($isadmin) {
            $button = array(
              '#type' => 'link',
              '#title' => t('Edit element settings'),
              '#url' => Url::fromRoute('entity.evalwf.settings_form.elements', ['webform' => $this->wfid,] ),
            );
            \Drupal::messenger()->addMessage(
              t('Evaluation settings of this webform haven\'t completed yet. Please fill and save Elements settings page...'),
              'warning'
            );
             \Drupal::messenger()->addMessage( $button, 'warning' );
          }
        }
        else {
          $form[]=array(
            '#type' => 'markup',
            '#markup' => t('NO DATA!').'<br>',
          );
        }
      }

      if ($this->entity->isDirectJump() && (!$isadmin)) {
        $conditions = unserialize($evaluation->getConditionsData());
        $responseurl = Url::fromUri($conditions[0]['url'])->setAbsolute(TRUE)->toString();
        if ( \Drupal::VERSION >= "9" ) {
          $response = new TrustedRedirectResponse( $responseurl, '302');
        }
        else {
          if (Url::fromUri($conditions[0]['url'])->setAbsolute(TRUE)->isExternal()) {
            return $conditions_form;
          }
          $response = new RedirectResponse( $responseurl, '302');
        }
        return $response;
      }
      else {
        if (!($this->entity->isRenderAsForm())) {
          if (!$this->entity->isIncludePTitle())
            unset( $build['#settings']['title'] );
          $this->setEvalWFId($this->wfid);
          $this->setEvaluationId($this->sid);
          $build['#cache']['tags'] = $this->getCacheTags();
          return $build;
        }
      }
    }
    else {
      $form[]=array(
        '#type' => 'markup',
        '#markup' => t('No evaluation settings were set to this request!').'<br>'
      );
      if ($isadmin) {
        $button = array(
          '#type' => 'link',
          '#title' => t('Add evaluation settings'),
          '#url' => Url::fromRoute('entity.evalwf.withpath.settings_form', ['webform' =>  $this->wfid,] ),
        );
        \Drupal::messenger()->addMessage(
          t('No Evaluation was set to this webform... Please add evaluation settings first...'),
          'warning'
        );
        \Drupal::messenger()->addMessage( $button, 'warning' );
      }
    }
    return $form;
  }

  /**
   *   Sends an email with these parameters
   *
   *   @param string from    - the sender email address
   *   @param string to      - the recipients
   *   @param string subject - the email subject
   *   @param string body    - the email body
   *   @param string mailer  - the mailmanager: phpmail/drupalmail
   */
  function send_mail($fromname, $from, $to, $subject, $body, $mailer = null ) {
    if ($this->entity->isRenderAsForm(true)) {
      $css = '<style>caption {text-align: left;font-weight: bold;font-size: 18px;}table {width:95%;padding-bottom:20px;}thead {background-color: #eee;}tfoot {background-color: #bbb;}.details-wrapper {border: 1px solid #aaa;margin-left: 20px;padding-left: 20px;}</style>';
    }
    else {
      $cssfile = $_SERVER['DOCUMENT_ROOT'] . base_path() . \Drupal::theme()->getActiveTheme()->getPath() . 'css/evalwf-resultpage-theme.css';
      if (file_exists($cssfile)) {
        $css = file_get_contents( $cssfile );
      }
      else {
        if (\Drupal::hasService('extension.list.module'))
          $cssfile = $_SERVER['DOCUMENT_ROOT'] . base_path() . \Drupal::service('extension.list.module')->getPath('evalwf') . '/css/evalwf-resultpage-theme.css';
        else
          $cssfile = $_SERVER['DOCUMENT_ROOT'] . base_path() . drupal_get_path('module', 'evalwf') . '/css/evalwf-resultpage-theme.css';
        if (file_exists($cssfile)) {
          $css = file_get_contents( $cssfile );
        }
      }
      if ($css!==FALSE) {
        $css = '<style>'. $css .'</style>';
      }
    }
    switch ($mailer) {
      case 'phpmail':
        $mailManager = new PhpMail();
        $message['headers'] = array(
          'content-type' => 'text/html',
          'MIME-Version' => '1.0',
          'charset' =>  'UTF-8',
          'reply-to' => $from,
          'From' => $fromname." <".$from.">",
        );
        $message['to'] = $to;
        $message['subject'] = $subject;
        $message['body'] = '<html><head>'. $css . '</head><body>' . $body . '</body></html>';
        $mailManager->mail($message);
      break;
      case 'drupalmail':
      default:
        $mailManager = \Drupal::service('plugin.manager.mail');
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $params['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
        $params['sendername'] = $fromname;
        $params['sender'] = $from;
        $params['subject'] = $subject;
        $params['message'] = Html::escape('<html><head>'. $css . '</head><body>' . $body . '</body></html>');
        $mailManager->mail('evalwf', 'Evaluation', $to, $langcode, $params, null, $send = TRUE);
      break;
    }
  }

  /**
   *   Recursively creates a form with webform elements and evaluation datas
   *
   *   @param array  items      - webform elements
   *   @param array  data       - webform submission data
   *   @param array  settings   - the evaluation settings data
   *   @returns array
   *        array  form             -  elementsform data
   *        string points           -  total points of all elements
   *        array  elements_points  -  elements and its evaluated points
   */
  function getElementsForm( $items, $data, $settings ) {
    $points = 0;
    $elementsform = [];
    $elements_points = [];
    foreach ($items as $key => $item ) {
      if (isset($item['#type'])) {
        $subpoints = 0;
        switch ( $item['#type']) {
          default:
              $plugin = $this->pluginManager->getPluginObjectFor($item['#type']);
              if (!empty($plugin)) {
                $builtdata = $plugin->buildResultFormData( $item, $data, $settings, $key, 0, $this->entity->isShowAll() );
                if ( !empty($builtdata['tree']) )
                  $elementsform[$key] = $builtdata['tree'][$key];

                if ( !empty($builtdata['subpoints']) )
                  $subpoints = $builtdata['subpoints'][$key];

                if( isset($builtdata['elements_points']) )
                  $elements_points = array_merge( $elements_points, $builtdata['elements_points'] );
                else
                  $elements_points = array_merge( $elements_points, array( $key => $subpoints ) );
              }
            break;
        }
        $points += $subpoints;
      }
    }
    return array(
      'form' => $elementsform,
      'points' => $points,
      'elements_points' => $elements_points
    );
  }

  /**
   *   Creates a form with evaluation conditions data (the continue link)
   *
   *   @param array  points      - elements and its evaluated points
   *   @returns form array
   */
  function getConditionsForm( $points ) {
    $conditions = null;
    if ( !empty($this->entity->getConditionsData()) )
      $conditions = unserialize($this->entity->getConditionsData())['table'];
    $form = [];
    $result = false;
    if ($conditions!=null) {
      foreach($conditions as $index => $condition ) {
        if ( is_numeric($index) && ($condition['telem']!='') && ($condition['url']!='') ) {
          if ((!$result) && ( $points[ $condition['telem'] ] >= $condition['lo'] ) && ( $points[ $condition['telem'] ] <= $condition['hi'] ) ) {
            $form[] = array(
              '#type' => 'link',
              '#title' => t('Continue').' >>>',
              '#url' => Url::fromUri($condition['url']) ,
              '#prefix' => '<br>',
            );
            $result = true;
          }
        }
      }
      if ((!$result) && (isset($conditions['elsecondition'])) && $conditions['elsecondition']['iselsecondition']) {
        $form[] = array(
          '#type' => 'link',
          '#title' => t('Continue').' >>>',
          '#url' => Url::fromUri($conditions['elsecondition']['elseurl']),
          '#prefix' => '<br>',
        );
      }
      return $form;
    }
  }

  /**
   *   Creates conditions data (the continue link data)
   *
   *   @param array  points      - elements and its evaluated points
   *   @returns form array
   */
  function getConditionsData( $points ) {
    $conditions = null;
    if ( !empty($this->entity->getConditionsData()) )
      $conditions = unserialize($this->entity->getConditionsData())['table'];
    $data = [];
    $result = false;
    if ($conditions!=null) {
      foreach($conditions as $index => $condition ) {
        if ( is_numeric($index) && ($condition['telem']!='') && ($condition['url']!='') ) {
          if ((!$result) && ( $points[ $condition['telem'] ] >= $condition['lo'] ) && ( $points[ $condition['telem'] ] <= $condition['hi'] ) ) {
            $data[] = array(
              'telem' => $condition['telem'],
              'lo' => $condition['lo'],
              'points' => $points[ $condition['telem'] ],
              'hi' => $condition['hi'],
              'url' => $condition['url'],
            );
            $result = true;
          }
        }
      }
      if ((!$result) && (isset($conditions['elsecondition'])) && ($conditions['elsecondition']['iselsecondition'])) {
        $data[] = array(
          'telem' => null,
          'lo' => null,
          'points' => null,
          'hi' => null,
          'url' => $conditions['elsecondition']['elseurl'],
        );
      }
      return $data;
    }
  }
}
