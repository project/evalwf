<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementPluginManager.php - Plugin Manager object for EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*
* @references: Jennifer Hodgdon: Programmer's Guide to Drupal
*/

namespace Drupal\evalwf\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Archiver plugin manager.
 */
class EvalWFElementPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/EvalWFElement',
      $namespaces,
      $module_handler,
      'Drupal\evalwf\Plugin\EvalWFElementPluginInterface',
      'Drupal\evalwf\Annotation\EvalWFElement'
    );
    $this->alterInfo('evalwfelement_info');
    $this->setCacheBackend($cache_backend, 'evalwfelement_plugins');
//    $this->logger = $logger;
  }

  protected function getType() {
    return 'evalwfelement';
  }

  public function getSortedDefinitions() {
    $definitions = $this->getDefinitions();
    sort($definitions);
    return $definitions;
  }

  /**
   *  Currently returns the prefered plugin if it is available or the first that contains the formelement_type
   *  @param: $type = webform element type
   */
  public function getPluginObjectFor( $type ) {

    $config = \Drupal::configFactory()->getEditable('evalwf.settings');
    $pluginsettings = empty($config->get('pluginsettings')) ? '' : unserialize($config->get('pluginsettings'));
    $this->clearCachedDefinitions();
    $definitions = $this->getDefinitions();
    $plugin_id = null;

    if ( isset($pluginsettings[$type]) && array_key_exists($pluginsettings[$type],$definitions) ) {
      $plugin_id = $pluginsettings[$type];
    }
    else {
      foreach ( $definitions as $key => $value ) {
        if (isset($value['types']) && is_array($value['types']) && in_array($type,$value['types']) && empty($plugin_id)) {
          $plugin_id = $key;
        }
      }
    }
    if (!empty( $plugin_id ))
      return $this->createInstance( $plugin_id );

    return null;
  }
}

?>
