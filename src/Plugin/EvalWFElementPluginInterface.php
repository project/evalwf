<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementPluginInterface.php - Plugin Interface object for EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*
* @references: Jennifer Hodgdon: Programmer's Guide to Drupal
*/

namespace Drupal\evalwf\Plugin;

interface EvalWFElementPluginInterface {

  public function label();
  public function getType();
//  public function access(AccountInterface $account, $return_as_object = FALSE);

  public function getShortDescription();
  public function getDetailedDescription();

  /**
   *  @returns: the form elements for the evalwf-settings-form
   */
  public function getSettingsForm( $item, $settings, $key );

  /**
   *  @returns: the result page data of the element for the twig template
   */
  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall );

  /**
   *  @returns: the result form data of the element
   */
  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall );

}
