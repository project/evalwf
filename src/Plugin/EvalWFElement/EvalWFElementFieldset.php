<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementFieldset.php - Fieldset EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_fieldset",
 *    label = @Translation("Fieldset EvalWFElement plugin"),
 *    types = {
 *      "container",
 *      "fieldset",
 *      "details",
 *    }
 *  )
 */
class EvalWFElementFieldset extends EvalWFElementBase implements EvalWFElementPluginInterface {

  function getSettingsForm( $item, $settings, $key ) {
    $form[$key]=array(
      '#type' => 'details',
      '#title' => $key,
      '#tree' => TRUE,
      '#open' => TRUE,
    );
    if ( !empty($this->pluginManager) ) {
      foreach ($item as $ckey => $child ) {
        if (isset($child['#type'])) {
          $child_plobject = $this->pluginManager->getPluginObjectFor($child['#type']);
          if ( !empty($child_plobject))
            $childdata = $child_plobject->getSettingsForm(
              $child,
              ( isset($settings[$key]) ? $settings[$key] : [] ),
              $ckey
            );
          $form[$key] = array_merge($form[$key], $childdata);
        }
      }
    }
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    $childs_data = [];
    $elements_points = [];
    if ( !empty($this->pluginManager) ) {
      foreach ($element as $ckey => $child ) {
        if (isset($child['#type'])) {
          $child_plobject = $this->pluginManager->getPluginObjectFor($child['#type']);
          if ( !empty($child_plobject)) {
            $childdata = $child_plobject->buildResultTwigData(
              $child,
              $data,
              ( isset($settings[$key]) ? $settings[$key] : null ),
              $ckey,
              0,
              $isshowall
            );
            if (isset($childdata['tree'])) {
              $childs_data[$ckey]['type'] = $child['#type'];
              $childs_data[$ckey]['title'] = $this->getTitle( $child );
              $childs_data[$ckey]['description'] = ( isset($child['#description']) ? $child['#description'] : '' );
              $childs_data[$ckey]['header'] = [ ['data'=>'', 'attributes'=>' width=5'],t('answer'),t('points'), ];
              $childs_data[$ckey] = array_merge($childs_data[$ckey],$childdata['tree'][$ckey]);
            }
            if (isset($childdata['elements_points']))
              $elements_points = array_merge( $elements_points, $childdata['elements_points'] );
            if (isset($childdata['subpoints']) && isset($childdata['subpoints'][$ckey])) {
              $elements_points = array_merge( $elements_points, [ $ckey => $childdata['subpoints'][$ckey] ] );
              $subpoints += $childdata['subpoints'][$ckey];
            }
          }
        }
      }
    }
    $tree[$key]['children'] = $childs_data;
    $tree[$key]['subtotal'] = t('Sum of @element: %subtotal Point(s)', [ '@element'=>$this->getTitle( $element ), '%subtotal'=>$subpoints] );

    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
      'elements_points' =>  array_merge( array( $key => $subpoints ), $elements_points ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    $childs_data = [];
    $elements_points = [];
    if ( !empty($this->pluginManager) ) {
      foreach ($item as $ckey => $child ) {
        if (isset($child['#type'])) {
          $child_plobject = $this->pluginManager->getPluginObjectFor($child['#type']);
          if ( !empty($child_plobject)) {
            $childdata = $child_plobject->buildResultFormData(
              $child,
              $data,
              ( isset($settings[$key]) ? $settings[$key] : null ),
              $ckey,
              0,
              $isshowall
            );
            if (isset($childdata['tree']))
              $childs_data[$ckey] = $childdata['tree'];
            if (isset($childdata['elements_points']))
              $elements_points = array_merge( $elements_points, $childdata['elements_points'] );
            if (isset($childdata['subpoints']) && isset($childdata['subpoints'][$ckey])) {
              $subpoints += $childdata['subpoints'][$ckey];
              $elements_points = array_merge( $elements_points, [ $ckey => $childdata['subpoints'][$ckey]] );
            }
          }
        }
      }
    }
    $elementsform[$key]=array(
      '#type' => 'details',
      '#title' => ( isset($item['#title']) ? $item['#title'] : '' ),
      '#tree' => TRUE,
      '#open' => TRUE,
      '#suffix' =>'<b>'. t('Subtotal of @key is: @subpoints points', [ '@key'=>$key, '@subpoints'=>$subpoints ]).'</b><br><br>',
    );
    $elementsform[$key][]= $childs_data;
    $formdata = array(
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
      'elements_points' => array_merge( array( $key => $subpoints ), $elements_points )
    );
    return $formdata;
  }

}
