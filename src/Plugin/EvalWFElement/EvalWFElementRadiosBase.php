<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementRadiosBase.php - RadiosBase abstract for radios type EvalWFElements
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

abstract class EvalWFElementRadiosBase extends EvalWFElementBase implements EvalWFElementPluginInterface {

  // For other element if presents
  protected $other = null;
  protected $other_plobject = null;

  function getPoints( $settings, $key, $optkey ) {
    $points = ( isset($settings[$key][$optkey]['points']) ? $settings[$key][$optkey]['points'] : null );
    return $points;
  }

  function isGood( $data, $key, $optkey ) {
    $isgood = isset($data[$key]) && ($optkey==$data[$key]);
    return $isgood;
  }

  function getSettingsForm( $item, $settings, $key ) {
    foreach($item['#options'] as $optkey => $optvalue ) {
      $cbinhtml[$optkey]= $this->createtablerow(
        $optvalue,
        $optkey,
        $this->getPoints( $settings, $key, $optkey )
      );
    }
    if (!empty($this->other) && isset($this->other[$key.'_other_t'][$key.'_other'])) {
      $cbinhtml[$key.'_other'] = $this->other[$key.'_other_t'][$key.'_other'];
    }
    $form[$key] = $this->createtable( $this->getTitle( $item ), array(t('available answers'),t('points')), $cbinhtml );
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    foreach($element['#options'] as $optkey => $optvalue ) {
      if (((!$isshowall) && $this->isGood( $data, $key, $optkey )) || ($isshowall)) {
        $tree[$key]['rows'][$optkey] = [
          'selected' => $this->isGood( $data, $key, $optkey ),
          'answer' => $optvalue,
          'points'=> (int)$this->getPoints( $settings, $key, $optkey ),
          'isgood'=> $this->isGood( $data, $key, $optkey ),
        ];
      }
      if ( $this->isGood( $data, $key, $optkey ) ) {
        $subpoints += (int)$this->getPoints( $settings, $key, $optkey );
      }
    }

    if (
      (!empty($this->other) && isset($this->other['tree'][$key.'_other']['rows'][$key.'_other'])) &&
      (((!$isshowall) && isset($data[$key]) && (!in_array($data[$key],$element['#options']))) || ($isshowall))
      ) {
          $tree[$key]['rows']['other'] = $this->other['tree'][$key.'_other']['rows'][$key.'_other'];
          $tree[$key]['rows']['other']['selected'] = (isset($data[$key]) ? !( in_array($data[$key],$element['#options']) ):false);
          $subpoints += $this->other['subpoints'][$key.'_other'];
        }

    $tree[$key]['footer'] = [[
      'data'=>t('@element: %subtotal Point(s)', [
        '@element'=> $this->getTitle( $element ),
        '%subtotal'=> $subpoints]
      ),
      'attributes'=>' colspan=3'
    ]];

    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    $optionsform = [];
    foreach($item['#options'] as $optkey => $optvalue ) {
      if (((!$isshowall) && $this->isGood( $data, $key, $optkey )) || ($isshowall)) {
        $optionsform[$optkey]= $this->createResultFormTableRow(
          array(
            '#type' => 'radio',
            '#name' => $key,
            '#return_value' => $optkey,
            '#value' => ( $this->isGood( $data, $key, $optkey ) ? $data[$key] : null),
            '#attributes' => array('disabled' => TRUE),
          ),
          $optvalue,
          (int)$this->getPoints( $settings, $key, $optkey )
        );
      }
      if ( $this->isGood( $data, $key, $optkey ) && !empty($this->getPoints( $settings, $key, $optkey ))) {
        $subpoints += (int)$this->getPoints( $settings, $key, $optkey );
      };
    }
    if (
      (!empty($this->other) && isset($this->other['tree'][$key.'_other'][$key.'_other'])) &&
      (((!$isshowall) && isset($data[$key]) && ( !in_array($data[$key],$item['#options']) )) || ($isshowall))
      ) {
      if (isset($this->other['tree'][$key.'_other'][$key.'_other']['item'])) {
        $optionsform['other'] = $this->other['tree'][$key.'_other'][$key.'_other'];
        $optionsform['other']['item']['#checked'] = (isset($data[$key]) ? !( in_array($data[$key],$item['#options']) ):false);
        $optionsform['other']['item']['#value'] = (isset($data[$key]) ? !( in_array($data[$key],$item['#options']) ):false);
      }
      else {
        $optionsform = array_merge( $optionsform, array (
          [
            'item' => array(
              '#type' => 'checkbox',
              '#name' => $key.'_other',
              '#value' => (isset($data[$key]) ? !( in_array($data[$key],$item['#options']) ):false),
              '#checked' => (isset($data[$key]) ? !( in_array($data[$key],$item['#options']) ):false),
              '#attributes' => array('disabled' => TRUE),
            ),
            $this->other[$key.'_other'][$key.'_other'][0],
          ]
        ));
      }
      if (!empty($this->other) && $this->other['subpoints'][$key.'_other'])
          $subpoints += $this->other['subpoints'][$key.'_other'];
    }
    $elementsform[$key]= $this->createResultFormTable(
      $this->getTitle( $item ),
      array( ['width' => 10, 'data' => ''],t('answer'),t('points') ),
      $optionsform,
      $subpoints
    );

    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

}
