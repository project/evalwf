<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementTableSort.php - Table_sort EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_table_sort",
 *    label = @Translation("Table_Sort EvalWFElement plugin"),
 *    types = {
 *      "webform_table_sort",
 *      "webform_tableselect_sort",
 *    }
 *  )
 */
class EvalWFElementTableSort extends EvalWFElementBase implements EvalWFElementPluginInterface {

  function getPoints( $settings, $key, $position ) {
    $points = ( isset($settings[$key][$key.'-'.$position]) && isset($settings[$key][$key.'-'.$position]['points']) ? $settings[$key][$key.'-'.$position]['points'] : null );
    return $points;
  }

  function isGood( $setting, $key, $optvalue ) {
    $isgood = ( isset($setting[$key]) ? ($setting[$key]==$optvalue ? t('true') : t('false'))  : '');
    return $isgood;
  }

  function getSettingsForm( $item, $settings, $key ) {
    $optionshtml=[];
    $count = 0;
    foreach($item['#options'] as $optkey => $optvalue ) {
      $cbinhtml[$key.'-'.++$count]= $this->createtablerow(
        t('The first %n item(s) ordered well', ['%n' => $count] ),
        $key.'-'.$count,
        (int)$this->getPoints( $settings, $key, $count )
      );
    }
    $form[$key]= $this->createtable( $this->getTitle( $item ), array(t('well ordered items count'),t('points')), $cbinhtml );
    $count = 0;
    if ( isset($settings[$key.'_order']) ) {
      foreach($settings[$key.'_order'] as $optkey => $optvalue ) {
        $optionshtml[$optkey] = array(
          ++$count,
          $item['#options'][$optvalue['value']],
        );
      }
    }
    else {
      foreach($item['#options'] as $optkey => $optvalue ) {
        $optionshtml[$optkey] = array(
          ++$count,
          $optvalue,
        );
      }
    }
    $form[$key.'_order'] = array(
        '#type' => 'webform_table_sort',
        '#header' => [t('No.'),t('Place the correct answers in good ordering')],
        '#options' => $optionshtml,
    );
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    $lastgood = 0;
    $tree[$key]['header'] = [ '',t('answer'),t('is at good place?') ];
    if (isset($data[$key])) {
      $i = 0;
      if (isset($settings[$key.'_order'])) {
        foreach ($settings[$key.'_order'] as $skey => $sval) {
          $setting[++$i] = $skey;
        }
      }
      $i = 0;
      foreach($data[$key] as $optkey => $optvalue ) {
        $tree[$key]['rows'][++$i] = [
            'order' => $i,
            'answer' => $element['#options'][$optvalue],
            'points' => (int)$this->getPoints( $settings, $key, $lastgood ),
            'isgood' => $this->isGood( $setting, $i, $optvalue ),
        ];
        if (($setting[$i]==$optvalue) && ($lastgood==($i-1))){
          $lastgood = $i;
        }
      }
      if ( ($lastgood!=0) && !empty($this->getPoints( $settings, $key, $lastgood )) ) {
        $subpoints += (int)$this->getPoints( $settings, $key, $lastgood );
      }
    }
    $tree[$key]['footer'] = [[
      'data' => t('The first %n item(s) ordered well: %points Point(s)', [
        '%n' => $lastgood,
        '%points' => $subpoints
      ]),
      'attributes' => ' colspan=3'
    ]];
    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    if (isset($data[$key])) {
      $i = 0;
      if (isset($settings[$key.'_order'])) {
        foreach ($settings[$key.'_order'] as $skey => $sval) {
          $setting[++$i] = $skey;
        }
      }
      $i = 0;
      $lastgood = 0;
      foreach($data[$key] as $optkey => $optvalue ) {
        $optionsform[$optkey]= $this->createResultFormTableRow(
          array(
            '#type' => 'markup',
            '#markup' => ++$i .'.',
          ),
          $item['#options'][$optvalue],
          $this->isGood( $setting, $i, $optvalue )
        );
        if (($setting[$i]==$optvalue) && ($lastgood==($i-1))){
          $lastgood = $i;
        }
      }
      if ( ($lastgood!=0) && !empty($this->getPoints( $settings, $key, $lastgood )) ) {
        $subpoints += (int)$this->getPoints( $settings, $key, $lastgood );
      }
      $elementsform[$key]= $this->createResultFormTable(
        $this->getTitle( $item ),
        array( ['width' => 10, 'data' => t('order')],t('answer'),t('is at good place?') ),
        $optionsform,
        $subpoints
      );
    }

    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

  public function getShortDescription() {
    return $this->getType() . ' - ' . get_class($this);
  }

}
