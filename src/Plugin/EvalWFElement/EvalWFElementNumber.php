<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementNumber.php - Number EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_number",
 *    label = @Translation("Number EvalWFElement plugin"),
 *    types = {
 *      "number",
 *      "range",
 *    }
 *  )
 */
class EvalWFElementNumber extends EvalWFElementBase implements EvalWFElementPluginInterface {

  function getPoints( $settings, $key ) {
    $points =( isset($settings[$key.'_t'][$key]['points']) ? $settings[$key.'_t'][$key]['points'] : null );
    return $points;
  }

  function getAnswer( $settings, $key ) {
    $answer = ( isset($settings[$key.'_t'][$key]['answer']) ? $settings[$key.'_t'][$key]['answer'] : null );
    return $answer;
  }

  function isGood( $settings, $data, $key ) {
    $goodvalues = [];
    $isgood = false;
    if (isset($settings[$key.'_t'][$key]['answer'])) {
      $goodvalues = explode(',',$settings[$key.'_t'][$key]['answer']);
    }
    array_walk($goodvalues,'trim');
    foreach ( $goodvalues as $value ) {
      $pos = strpos( $value, '-');
      if ($pos == false) {
        if (isset($data[$key]) && ($value == $data[$key]) ) {
          $isgood = true;
        }
      }
      else {
        $range = explode('-',$value);
        array_walk($range,'trim');
        if ( isset($data[$key]) && ($range[0]<=$data[$key]) && ($data[$key] <= $range[1]) ) {
          $isgood = true;
        }
      }
    }
    return $isgood;
  }

  function getSettingsForm( $item, $settings, $key ) {
    $cbinhtml[$key] = array(
      'answer' => array(
        '#type' => 'textfield',
        '#id' => $key,
        '#title' => t('Good answers') . (isset($item['#title']) ? ' '.t('for @item', [ '@item'=>$item['#title'] ]) : '' ) . ':',
        '#value' => $this->getAnswer( $settings, $key ),
        '#description' => t('You can list them separated with coma and you can use ranges too. e.g.: 1,4,11-15,22'),
        '#element_validate' => [[$this, 'NumberListValidate']],
      ),
      'points' => array(
        '#type' => 'textfield',
        '#id' => $key,
        '#size' => 5,
        '#value' => $this->getPoints( $settings, $key ),
        '#element_validate' => [[$this, 'PointsInputValidate']],
      ),
    );
    $form[$key.'_t']= $this->createtable( $this->getTitle( $item ), array(t('available answers'),t('points')), $cbinhtml );
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    $isgood = $this->isGood( $settings, $data, $key );
    $tree[$key]['rows'][$key] = [
      'selected' => $isgood,
      'answer' => (isset($data[$key]) ? $data[$key] : $this->getTitle( $element )),
      'points' => ($isgood ? (int)$this->getPoints( $settings, $key ) : 0 ),
      'isgood' => $isgood,
    ];
    if ( $isgood ) {
        $subpoints += (int)$this->getPoints( $settings, $key );
    }
    $tree[$key]['footer'] = [[
      'data'=>t('@element: %subtotal Point(s)', [
        '@element'=> $this->getTitle( $element ),
        '%subtotal'=> $subpoints]
      ),
      'attributes'=>' colspan=3'
    ]];

    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    $isgood = $this->isGood( $settings, $data, $key );
    $optionsform[$key]= $this->createResultFormTableRow(
      array(
        '#type' => 'checkbox',
        '#name' => $key,
        '#value' => $isgood,
        '#checked' => $isgood,
        '#attributes' => array('disabled' => TRUE),
      ),
      (isset($data[$key]) ? $data[$key] : $this->getTitle( $item )),
      ($isgood ? (int)$this->getPoints( $settings, $key ) : 0 )
    );
    if ( $isgood ) {
        $subpoints += (int)$this->getPoints( $settings, $key );
    }
    $elementsform[$key]= $this->createResultFormTable(
      $this->getTitle( $item ),
      array( ['width' => 10, 'data' => ''],t('answer'),t('points') ),
      $optionsform,
      $subpoints
    );

    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

  public function getShortDescription() {
    return $this->getType() . ' - ' . get_class($this);
  }

  function NumberListValidate( $element, $form_state) {
    $i = $form_state->getUserInput()['elements'][0];
    $v = explode( ',', $this->mygetValue( $i , $element['#id'] )['answer']);
    array_walk($v,'trim');

    foreach ( $v as $key => $item ) {
      if( !is_numeric($item) ) {
        $r = explode('-', $item);
        array_walk($r,'trim');
        if ( (count($r)!=2) || !is_numeric($r[0]) || !is_numeric($r[1]) ) {
          $form_state->seterror($element, t('You must enter valid numbers or ranges in list separated with comas. You must declare ranges as from-to. e.g.: 1,5,10-12,100'));
        }
      }
    };
  }

}
