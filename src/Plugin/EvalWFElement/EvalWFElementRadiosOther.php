<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementRadiosOther.php - Radios_other EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElement\EvalWFElementRadiosBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_radios_other",
 *    label = @Translation("radios_other EvalWFElement plugin"),
 *    types = {
 *      "webform_radios_other",
 *      "webform_select_other",
 *    }
 *  )
 */
class EvalWFElementRadiosOther extends EvalWFElementRadiosBase implements EvalWFElementPluginInterface {

  function getOtherElementType( $item ) {
    $other_type = ( isset($item['#other__type']) ? $item['#other__type'] : 'textfield' );
    return $other_type;
  }

  function getOtherElementTitle( $item ) {
    $other_title = ( isset($item['#other__title']) ? $item['#other__title'] : null );
    return $other_title;
  }

  function getOtherElementDescription( $item ) {
    $other_description = ( isset($item['#other__description']) ? $item['#other__description'] : null );
    return $other_description;
  }

  function getOtherElementSize( $item ) {
    $other_size = ( isset($item['#other__size']) ? $item['#other__size'] : null );
    return $other_size;
  }

  function getOtherElement( $item, $key ) {
    $other_element = [ $key.'_other' => array(
      '#type' => $this->getOtherElementType( $item ),
      '#title' => $this->getOtherElementTitle( $item ),
      '#description' => $this->getOtherElementDescription( $item ),
      '#size' => $this->getOtherElementSize( $item ),
    )];
    return $other_element;
  }

  function getSettingsForOtherElement( $settings, $key ) {
    $settings_other = ( isset($settings[$key][$key.'_other']) ? [$key.'_other_t' => [ $key.'_other' => $settings[$key][$key.'_other']]] : [] );
    return $settings_other;
  }

  function getSettingsForm( $item, $settings, $key ) {
    if ( !empty($this->pluginManager) ) {
      $this->other_plobject = $this->pluginManager->getPluginObjectFor($this->getOtherElementType( $item ));
      if ( !empty($this->other_plobject))
        $this->other = $this->other_plobject->getSettingsForm(
          $this->getOtherElement( $item, $key ),
          $this->getSettingsForOtherElement( $settings, $key ),
          $key.'_other'
        );
    }
    $form = parent::getSettingsForm($item, $settings, $key);
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    if ( !empty($this->pluginManager) ) {
      $this->other_plobject = $this->pluginManager->getPluginObjectFor($this->getOtherElementType( $element ));
      if ( !empty($this->other_plobject))
        $this->other = $this->other_plobject->buildResultTwigData(
          $this->getOtherElement( $element, $key ),
          [$key.'_other' => (isset($data[$key]) && (!in_array($data[$key],$element['#options'])) ? $data[$key] : null )],
          $this->getSettingsForOtherElement( $settings, $key ),
          $key.'_other',
          $subpoints,
          $isshowall
        );
    }
    $builtdata = parent::buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall );
    return $builtdata;
  }


  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    if ( !empty($this->pluginManager) ) {
      $this->other_plobject = $this->pluginManager->getPluginObjectFor($this->getOtherElementType( $item ));
      if ( !empty($this->other_plobject))
        $this->other = $this->other_plobject->buildResultFormData(
          $this->getOtherElement( $item, $key ),
          [$key.'_other' => (isset($data[$key]) && (!in_array($data[$key],$item['#options'])) ? $data[$key] : null )],
          $this->getSettingsForOtherElement( $settings, $key ),
          $key.'_other',
          $subpoints,
          $isshowall
        );
    }
    $formdata = parent::buildResultFormData($item, $data, $settings, $key, $subpoints, $isshowall);
    return $formdata;
  }

  public function getShortDescription() {
    return $this->getType() . ' - ' . get_class($this);
  }

}
