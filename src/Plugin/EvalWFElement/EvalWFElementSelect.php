<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementSelect.php - Select EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElement\EvalWFElementRadiosBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_select",
 *    label = @Translation("Select EvalWFElement plugin"),
 *    types = {
 *      "select",
 *    }
 *  )
 */
class EvalWFElementSelect extends EvalWFElementRadiosBase implements EvalWFElementPluginInterface {

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    foreach($item['#options'] as $optkey => $optvalue ) {
      if (((!$isshowall) && $this->isGood( $data, $key, $optkey )) || ($isshowall)) {
        $optionsform[$optkey]= $this->createResultFormTableRow(
          array(
            '#type' => 'checkbox',
            '#name' => $key,
            '#value' => $this->isGood( $data, $key, $optkey ),
            '#checked' => $this->isGood( $data, $key, $optkey ),
            '#attributes' => array('disabled' => TRUE),
          ),
          $optvalue,
          (int)$this->getPoints( $settings, $key, $optkey )
        );
      }
      if ( $this->isGood( $data, $key, $optkey ) && !empty($this->getPoints( $settings, $key, $optkey )) ) {
        $subpoints += (int)$this->getPoints( $settings, $key, $optkey );
      }
    }
    $elementsform[$key]= $this->createResultFormTable(
      $this->getTitle( $item ),
      array( ['width' => 10, 'data' => ''],t('answer'),t('points') ),
      $optionsform,
      $subpoints
    );

    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

  public function getShortDescription() {
    return $this->getType() . ' - ' . get_class($this);
  }

}
