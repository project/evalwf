<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementCheckbox.php - Checkbox EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_checkbox",
 *    label = @Translation("Checkbox EvalWFElement plugin"),
 *    types = {
 *      "checkbox",
 *    }
 *  )
 */
class EvalWFElementCheckbox extends EvalWFElementBase implements EvalWFElementPluginInterface {

  function getPoints( $settings, $key ) {
    $points =( isset($settings[$key.'_t'][$key]['points']) ? $settings[$key.'_t'][$key]['points'] : null );
    return $points;
  }

  function isGood( $data, $key ) {
    $isgood = isset($data[$key]) && ($data[$key]);
    return $isgood;
  }

  function getSettingsForm( $item, $settings, $key ) {
    $cbinhtml[$key] = $this->createtablerow(
      $this->getTitle( $item ),
      $key,
      $this->getPoints( $settings, $key )
    );
    $form[$key.'_t']= $this->createtable( $this->getTitle( $item ), array(t('available answers'),t('points')), $cbinhtml );
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    $tree[$key]['rows'] = [
      $key => [
        'selected' => $this->isGood( $data, $key ),
        'answer' => $this->getTitle( $element ),
        'points' => $this->getPoints( $settings, $key ),
        'isgood' => $this->isGood( $data, $key ),
      ],
      ];
    if ($this->isGood( $data, $key )) {
      $subpoints += (int)$this->getPoints( $settings, $key );
    }
    $tree[$key]['footer'] = [[
      'data'=>t('@element: %subtotal Point(s)', [ '@element'=>$this->getTitle( $element ), '%subtotal'=> $subpoints]),
      'attributes'=>' colspan=3',
    ]];
    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    $optionsform[$key]= $this->createResultFormTableRow(
      array(
        '#type' => 'checkbox',
        '#name' => $key,
        '#value' => $this->isGood( $data, $key ),
        '#checked' => $this->isGood( $data, $key ),
        '#attributes' => array('disabled' => TRUE),
      ),
      $this->getTitle( $item ),
      $this->getPoints( $settings, $key )
    );
    if ( $this->isGood( $data, $key ) && !empty($this->getPoints( $settings, $key )) ) {
      $subpoints += (int)$this->getPoints( $settings, $key );
    }
    $elementsform[$key]= $this->createResultFormTable(
      $this->getTitle( $item ),
      array( ['width' => 10, 'data' => ''],t('answer'),t('points') ),
      $optionsform,
      $subpoints
    );
    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

  public function getShortDescription() {
    return $this->getType() . ' - ' . get_class($this);
  }

}
