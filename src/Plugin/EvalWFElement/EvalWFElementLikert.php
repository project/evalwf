<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementLikert.php - Webform Likert EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_likert",
 *    label = @Translation("Webform_Likert EvalWFElement plugin"),
 *    types = {
 *      "webform_likert",
 *    }
 *  )
 */
class EvalWFElementLikert extends EvalWFElementBase implements EvalWFElementPluginInterface {

  function getPoints( $settings, $key, $qkey, $ankey ) {
    $points = ( isset($settings[$key][$qkey.'-'.$ankey]['points']) ? $settings[$key][$qkey.'-'.$ankey]['points'] : null );
    return $points;
  }

  function isGood( $data, $key, $qkey, $ankey ) {
    $isgood = isset($data[$key]) && isset($data[$key][$qkey]) && ($ankey == $data[$key][$qkey]);
    return $isgood;
  }

  function isSelected( $data, $key, $qkey, $ankey ) {
    $isselected = (isset($data[$key][$qkey]) ? $ankey == $data[$key][$qkey] : false);
    return $isselected;
  }

  function getSettingsForm( $item, $settings, $key ) {
    foreach($item['#questions'] as $qkey => $qvalue ) {
      $cbinhtml[$qkey] = array(
        'item' => array(
          '#type' => 'markup',
          '#markup' => $qvalue,
          '#wrapper_attributes' => array(
            'colspan' => 3,
          ),
        ),
      );
      foreach($item['#answers'] as $ankey => $anvalue ) {
        $cbinhtml[$qkey.'-'.$ankey]= $this->createtablerow(
          '&nbsp;&nbsp;&nbsp;&nbsp;' . $anvalue,
          $qkey.'-'.$ankey,
          $this->getPoints( $settings, $key, $qkey, $ankey)
        );
      }
    }
    $form[$key]= $this->createtable( $this->getTitle( $item ), array(t('available answers'),t('points')), $cbinhtml );
    return $form;
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    foreach($element['#questions'] as $qkey => $qvalue ) {
      $tree[$key]['rows'][$qkey] = [
        'question' => ['data'=>$qvalue, 'attributes'=>' colspan=3']
      ];
      foreach($element['#answers'] as $ankey => $anvalue ) {
        if (((!$isshowall) && $this->isGood( $data, $key, $qkey, $ankey)) || ($isshowall)) {
          $tree[$key]['rows'][$qkey.'-'.$ankey] = [
            'selected' => $this->isSelected( $data, $key, $qkey, $ankey),
            'answer' => $anvalue,
            'points' => (int)$this->getPoints( $settings, $key, $qkey, $ankey),
            'isgood' => $this->isGood( $data, $key, $qkey, $ankey)
          ];
        }
        if ( $this->isGood( $data, $key, $qkey, $ankey) ) {
          $subpoints += (int)$this->getPoints( $settings, $key, $qkey, $ankey);
        }
      }
    }
    $tree[$key]['footer'] = [[
      'data'=>t('@element: %subtotal Point(s)', [ '@element'=> $this->getTitle( $element ), '%subtotal'=> $subpoints]),
      'attributes'=>' colspan=3'
    ]];
    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    foreach($item['#questions'] as $qkey => $qvalue ) {
      $optionsform[$qkey] = array(
        'item' => array(
          '#type' => 'markup',
          '#markup' => $qvalue,
          '#wrapper_attributes' => array(
            'colspan' => 3,
          ),
        ),
      );
      foreach($item['#answers'] as $ankey => $anvalue ) {
        if (((!$isshowall) && $this->isGood( $data, $key, $qkey, $ankey)) || ($isshowall)) {
          $optionsform[$qkey.'-'.$ankey]= $this->createResultFormTableRow(
            array(
              '#type' => 'checkbox',
              '#name' => $qkey,
              '#value' => $this->isSelected( $data, $key, $qkey, $ankey),
              '#checked' => $this->isSelected( $data, $key, $qkey, $ankey),
              '#attributes' => array('disabled' => TRUE),
            ),
            $anvalue,
            (int)$this->getPoints( $settings, $key, $qkey, $ankey)
          );
        }
        if ( $this->isGood( $data, $key, $qkey, $ankey) && !empty($this->getPoints( $settings, $key, $qkey, $ankey)) ) {
          $subpoints += (int)$this->getPoints( $settings, $key, $qkey, $ankey);
        }
      }
    }
    $elementsform[$key]= $this->createResultFormTable(
      $this->getTitle( $item ),
      array( ['width' => 10, 'data' => ''],t('answer'),t('points') ),
      $optionsform,
      $subpoints
    );

    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

  public function getShortDescription() {
    return $this->getType() . ' - ' . get_class($this);
  }

}
