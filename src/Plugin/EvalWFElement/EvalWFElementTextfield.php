<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementTextfield.php - Textfield EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*/

namespace Drupal\evalwf\Plugin\EvalWFElement;

use Drupal\evalwf\Plugin\EvalWFElementBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

/**
 *  @EvalWFElement(
 *    id = "evalwf_textfield",
 *    label = @Translation("Textfield EvalWFElement plugin"),
 *    types = {
 *      "textfield",
 *      "textarea",
 *      "email",
 *      "webform_autocomplete",
 *    }
 *  )
 */
class EvalWFElementTextfield extends EvalWFElementBase implements EvalWFElementPluginInterface {

  function getSettingsForm( $item, $settings, $key ) {
    return [];
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    $tree[$key]['header'] = null;
    $tree[$key]['text'] = ( isset($data[$key]) ? $data[$key] : null );

    $builtdata = [
      'tree' => $tree,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $builtdata;
  }

  public function buildResultFormData( $item, $data, $settings, $key, $subpoints, $isshowall ) {
    $elementsform[$key] = array(
      '#type' => 'markup',
      '#markup' => '<b>'. $this->getTitle($item) .'</b>',
      '#suffix' => '<br>',
    );
    $elementsform[$key][$key] = [
      array(
        '#type' => 'markup',
        '#markup' => ( isset($data[$key]) ? $data[$key] : null ),
        '#wrapper_attributes' => array(
          'colspan' => 2,
        ),
      )
    ];

    $formdata = [
      'tree' => $elementsform,
      'subpoints' => array( $key => $subpoints ),
    ];
    return $formdata;
  }

}
