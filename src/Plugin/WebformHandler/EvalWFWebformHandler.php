<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Plugin\WebformHandler\EvalWFWebformHandler.php - Addon handler to webforms for automatic evaluation with EvalWF module
*
* @author Tóthpál István
*
* @references: \Drupal\webform\Plugin\WebformHandler\*
*/

namespace Drupal\evalwf\Plugin\WebformHandler;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandlerBase;

use Drupal\evalwf\Controller\EvalWFController;

/**
 * Handler for automatic evaluation with EvalWF
 *
 * @WebformHandler(
 *   id = "EvalWF_Evaluation",
 *   label = @Translation("Evaluation"),
 *   category = @Translation("Evaluation"),
 *   description = @Translation("Allows automatic Webform evaluation through EvalWF module."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class EvalWFWebformHandler extends WebformHandlerBase {

  public function defaultConfiguration() {
    $res = parent::defaultConfiguration();
    return $res;
  }

  public function getConfigurationElement( $element, $rtype = null ) {
    if ($rtype=='bool')
      return isset($this->configuration[$element]) ? (
          !empty($this->configuration[$element]) ? boolval($this->configuration[$element]) : false
        ) :
        false;
    return isset($this->configuration[$element]) ? $this->configuration[$element] : '';
  }
  public function setConfigurationElement( $element, array $input, $type = null ) {
    if ($type=='bool')
      $this->configuration[$element] = isset($input[$element]) ? $input[$element] : false;
    else
      $this->configuration[$element] = isset($input[$element]) ? $input[$element] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();
    $wfid = $webform->id();
    $form['evalwf'] = [
      '#type' => 'details',
      '#title' => t('EvalWF Evaluation settings'),
    ];

    $query = \Drupal::entityQuery('evalwf')->accessCheck(FALSE)->condition('webform_id', $wfid);
    $evalwfs = $query->execute();
    $evalid = null;
    $evalval= null;
    if (!empty($evalwfs)) {
      foreach ($evalwfs as $key => $value) {
        $evalid = $key;
        $evalval= $value;
      }
    }
    $form['evalwf']['evalchk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The selected Webform\'s EvalWF Evaluation'),
      '#maxlength' => 255,
      '#default_value' => (!empty($evalwfs)) ? $evalval : '-- NOT SET --',
      '#description' => $this->t("The selected Webform's EvalWF Evaluation if it is already set."),
      '#disabled' => TRUE,
    ];
    $form['evalwf']['setup'] = [
      '#type' => 'link',
      '#title' => $this->t('Add/Manage EvalWF settings'),
      '#url' => Url::fromRoute( 'entity.evalwf.withpath.settings_form', ['webform' => $wfid] ),
      '#attributes' => ['target' => '_blank'],
    ];

    $form['evalwf']['warn'] = [
      '#type' => 'markup',
      '#markup' => '<p>&nbsp;</p><p>'.$this->t('This handler may block Webforms confirmation redirect, try not displaying result page in that case...').'</p>',
    ];
    $form['evalwf']['dspresult'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not display result page') . '<p>&nbsp;</p>',
      '#default_value' => $this->getConfigurationElement( 'dspresult', 'bool' ),
    ];

    $form['evalwf']['confirmation_linkdesc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('A description message about confirmation link'),
      '#maxlength' => 255,
      '#default_value' => $this->getConfigurationElement( 'confirmation_linkdesc' ),
      '#description' => $this->t("This message will be displayed above the selected Webform's confirmation link."),
    ];
    $form['evalwf']['confirmation_linktext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The link text for confirmation link'),
      '#maxlength' => 255,
      '#default_value' => $this->getConfigurationElement( 'confirmation_linktext' ),
      '#description' => $this->t("This text will be displayed as a link to the selected Webform's confirmation url."),
    ];

    $form['evalwf']['confirmation_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The selected Webform\'s confirmation type'),
      '#maxlength' => 255,
      '#default_value' => $webform->getSettings()['confirmation_type'],
      '#description' => $this->t("The selected Webform's confirmation type. It should be changed to url and set the url as below for automatic evaluation."),
      '#disabled' => TRUE,
    ];
    $form['evalwf']['evalconfurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The selected Webform\'s confirmation url'),
      '#maxlength' => 255,
      '#default_value' => $webform->getSettings()['confirmation_url'],
      '#description' => $this->t("The selected Webform's confirmation url. It should be changed to evalwf/[webform:id]/[webform_submission:sid]  or add EvalWF_Evaluation handler for automatic evaluation."),
      '#disabled' => TRUE,
    ];
    $form['evalwf']['chsettings'] = [
      '#type' => 'link',
      '#title' => t('Change this settings'),
      '#url' => Url::fromRoute( 'entity.webform.settings_confirmation', ['webform' => $wfid] ),
      '#attributes' => ['target' => '_blank'],
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $input = isset($form_state->getUserInput()['settings']['evalwf']) ? $form_state->getUserInput()['settings']['evalwf'] : null;
    if ($input!=null) {
      $this->setConfigurationElement( 'dspresult', $input, 'bool' );
      $this->setConfigurationElement( 'confirmation_linkdesc', $input );
      $this->setConfigurationElement( 'confirmation_linktext', $input );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#settings' => $this->configuration,
      '#handler' => $this,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $sid = $webform_submission->id();
    $wfid = $webform_submission->bundle();

    $webform = $this->getWebform();
    if ($webform!=null) {
      $ctype = $webform->getSettings()['confirmation_type'];
      $curl = $webform->getSettings()['confirmation_url'];
    }

    if (!$this->getConfigurationElement( 'dspresult', 'bool' )) {
      if ( isset($ctype) && isset($curl) && ($ctype=='url') && ($curl!='evalwf/[webform:id]/[webform_submission:sid]') ) {
        if (!in_array( explode('://', trim($curl) )[0],['http','https']) && !in_array( explode(':/', trim($curl) )[0],['internal'])) {
          $curl = 'internal:/' . $curl;
        }
        $url = Url::fromUri( $curl )->setAbsolute(TRUE)->setOption( 'attributes', array( 'target' => '_blank' ));

        if ($this->getConfigurationElement( 'confirmation_linkdesc', 'bool' )) {
          \Drupal::messenger()->addMessage($this->getConfigurationElement( 'confirmation_linkdesc' ));
        }
        if ($this->getConfigurationElement( 'confirmation_linktext', 'bool' )) {
          \Drupal::messenger()->addMessage(
            Link::fromTextAndUrl($this->getConfigurationElement( 'confirmation_linktext' ), $url )
          );
        }
        else {
          \Drupal::messenger()->addMessage(
            Link::fromTextAndUrl(t('Open confirmation url'), $url )
          );
        }
      }

      $target = Url::fromRoute('evalwf.withpath.content', [
          'webform' => $wfid, 'webform_submission' => $sid
        ])->setAbsolute(TRUE)->toString();

      $response = new TrustedRedirectResponse( $target, '302');
      $response->send();
    }
    else {
        $mycontroller = new EvalWFController;
        $wf = \Drupal::entityTypeManager()->getStorage('webform')->load($wfid);
        $wfs = \Drupal::entityTypeManager()->getStorage('webform_submission')->load($sid);
        $form = $mycontroller->content( $wf, $wfs, false );
    }
  }

}
