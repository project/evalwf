<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file EvalWFElementBase.php - abstract object for EvalWFElement
*
* @author Tóthpál István <istvan@tothpal.eu>
*
* @references: Jennifer Hodgdon: Programmer's Guide to Drupal
*/

namespace Drupal\evalwf\Plugin;

use Drupal\Core\Plugin\PluginBase;
//use Drupal\Core\Plugin\ContextAwarePluginBase;
use Drupal\evalwf\Plugin\EvalWFElementPluginInterface;

abstract class EvalWFElementBase extends PluginBase implements EvalWFElementPluginInterface {

  protected $pluginManager;

  public function __construct() {
    $this->pluginManager = \Drupal::getContainer()->get('plugin.manager.evalwfelement');
  }

  public function label() {
    $definition = $this->getPluginDefinition();
    return ( isset($definition['label']) ? (string) $definition['label'] : '' );
  }
  public function getType() {
    $definition = $this->getPluginDefinition();
    return ( isset($definition['id']) ? (string) $definition['id'] : '' );
  }

  function getTitle( $element ) {
    $title = isset($element['#title']) ? $element['#title'] : ( isset($element['#caption']) ? $element['#caption'] : '' );
    return $title;
  }

  public function getShortDescription() {
    return t( '- No short description were set - %type : %class', [ '%type' => $this->getType(), '%class' => get_class($this) ] );
  }

  public function getDetailedDescription() {
    return t( '- No detailed description were set - %type : %class', [ '%type' => $this->getType(), '%class' => get_class($this) ] );
  }

  public function buildResultTwigData( $element, $data, $settings, $key, $subpoints, $isshowall ) {
    return [];
  }

   /**
   *    Creates a table with header and caption with @param rows.
   *    @param string title     - the caption of table
   *    @param array header     - an array of header elements
   *    @param array rowsform   - an array of rows (each row an array of row item's form elements)
   *    @returns form of table in array
   */
  function createtable( $title, $header, $rowsform ) {
    $tableform = array(
      '#type' => 'table',
      '#caption' => $title,
      '#header' => $header,
      '#prefix' => '<br>',
    );
    $tableform = array_merge( $tableform, $rowsform );
    return $tableform;
  }

   /**
   *    Creates a table row with answer description and an input field for points
   *    @param string label   - the description of answer
   *    @param string key     - the input field's id
   *    @param string value   - the input field's starting value
   *    @returns form of tablerow in array
   */
  function createtablerow( $label, $key, $value ) {
    $rowform = array(
      'answer' => array(
        '#type' => 'markup',
        '#markup' => $label,
      ),
      'points' => array(
        '#type' => 'textfield',
        '#id' => $key,
        '#size' => 5,
        '#value' => $value,
        '#element_validate' => [[$this, 'PointsInputValidate']],
      ),
    );
    return $rowform;
  }

   /**
   *    Recursively find the item in imput elements and returns points array
   *    @param array  i     - the input elements array
   *    @param string key   - the items key
   *    @returns points array
   */
  function mygetValue($i, $key) {
      $value = null;
      foreach ( $i as $k => $v ) {
        if ($k==$key) { $value=$v; }
        else {
          if (is_array($v)) {
            if ($value==null) { $value = $this->mygetValue($v, $key); }
          }
        }
      }
      return $value;
    }

  function PointsInputValidate( $element, $form_state) {
    $i = $form_state->getUserInput()['elements'][0];
    $v = $this->mygetValue( $i , $element['#id'] );
    if ( !is_numeric( $v['points'] ) ) {
      $form_state->seterror($element, t('You must enter valid numbers.'));
    }
  }

  /**
   *   Creates a table with evaluation data for one webform item
   *
   *   @param string title    - the title/caption of the table
   *   @param array  header   - the header items of the table
   *   @param array  rowsform - the rows in form array
   *   @param string total    - the total points of this item
   *   @returns form array
   */
  function createResultFormTable( $title, $header, $rowsform, $total ) {
    $tableform = array(
      '#type' => 'table',
      '#caption' => $title,
      '#header' => $header,
      '#prefix' => '<br>',
      '#footer' => array(
        'data' => array(
          array(
            'colspan' => 2,
            'data' => array(
              '#type' => 'markup',
              '#markup' => '<b>'. t('Subtotal:') .'</b>',
            ),
          ),
          array(
            'data' => array(
              '#type' => 'markup',
              '#markup' => '<b>'. $total .'</b>',
            ),
          ),
        )
      ),
    );
    if ( $rowsform ) { $tableform = array_merge( $tableform, $rowsform ); }
    return $tableform;
  }

  /**
   *   Creates a row with evaluation data for one webform item option/question/answer
   *
   *   @param array  item      - checkbox/radio item in form array
   *   @param string label     - the label for this option
   *   @param string value     - the points of this option
   *   @returns form array
   */
  function createResultFormTableRow( $item, $label, $value ) {
    $rowform = array(
      'item'   => $item,
      'answer' => array(
        '#type' => 'markup',
        '#markup' => $label,
      ),
      'points' => array(
        '#type' => 'markup',
        '#markup' => $value,
      ),
    );
    return $rowform;
  }

}
