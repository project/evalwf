<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Form\EvalWFForm.php - Creates admin side forms to manage evaluation settings
*
* @author Tóthpál István
*
* @tutorial: "Creating a configuration entity type in Drupal 8" tutorial
*      @url: https://www.drupal.org/node/1809494
*/

namespace Drupal\evalwf\Form;

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Entity\Webform;
use Drupal\evalwf\Helper\EvalWFTxtHelper;

/**
 * Form handler for the EvalWFForm.
 */
class EvalWFForm extends EntityForm {

  protected $entity = null;

  protected $pluginManager;

  public static function create(ContainerInterface $container) {
    $e = parent::create($container);
    $e->pluginManager = $container->get('plugin.manager.evalwfelement');
    return $e;
  }

  /**
   *    Trying to load evalwf Entity
   *    @returns entity
   */
  public function getEntity() {
    $webform = $this->getRequest()->attributes->get('webform');
    if ($webform!=null) {
      $webform_id=$webform->id();
      $query = \Drupal::entityQuery('evalwf')->accessCheck(TRUE)->condition('webform_id', $webform_id);
      $entities = $query->execute();
      if (!empty($entities))
        $eid = reset($entities);
      if(isset($eid) && ($eid!=null)) $this->entity = $this->entity->load($eid);
    }
    return $this->entity;
  }

  /**
   *    Get Webform Object from this form object ( for compatibility with webform_ui_element_form )
   *    @returns webform object
   */
  public function getWebform() {
    return $this->getRequest()->attributes->get('webform');
  }

  /**
   *    Creates the form for evalwf settings
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   *    @returns form array
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $webform = $this->getRequest()->attributes->get('webform');
    if ($webform!=null) {
      $webform_id=$webform->id();
      $this->getEntity();
    }
    $myentity = $this->entity;
    if (isset($webform_id) && ($myentity->getWebformId()!=$webform_id))
      $myentity->setWebformId($webform_id);

    $query = \Drupal::entityQuery('webform')->accessCheck(FALSE);
    $wfids = $query->execute();
    $options = array();

    \Drupal::messenger()->addMessage( t("Please, don't forget to save changes on each tab before switching tab!"), 'warning');

    switch ($form_state->getFormObject()->operation) {
      case 'add':
        $this->testWebform($myentity->getWebformID());
        $form[] = [
          '#type' => 'markup',
          '#markup' => t('Please select webform for evaluation settings:').'<br>',
        ];
        foreach($wfids as $key => $value ) {
          $wf = Webform::load($key);
          $query = \Drupal::entityQuery('evalwf')->accessCheck(FALSE)->condition('webform_id', $key);
          $entitys = $query->execute();
          $row[$key] = array(
            'hasevalwf' => array(
              '#type' => 'markup',
              '#markup' => ( $entitys ? '&#10004;' : '+'),
            ),
            'webform_id' => array(
              '#type' => 'link',
              '#title' => $value,
              '#url' => ( in_array($key, $this->error_elements_list) ) ? Url::fromRoute('entity.webform.collection') :
                Url::fromRoute('entity.evalwf.withpath.settings_form', ['webform' => $key,] ),
            ),
            'description' => array(
              '#type' => 'markup',
              '#markup' => ( in_array($key, $this->error_elements_list) ) ? t('Error! Please modify webform/elements key to unique and not numeric!') : $wf->getDescription(),
            ),
            'owner' => array(
              '#type' => 'markup',
              '#markup' => ($wf->getOwner() ? $wf->getOwner()->getAccountName() : '' ),
            ),
          );
          if (in_array($key, $this->error_elements_list)) {
            $row[$key]['description']['#wrapper_attributes']['class'] = 'messages messages--error';
          }
        }
        $form['list'] = array(
          '#type' => 'table',
          '#caption' => '',
          '#header' => array( [ 'width' => 10, 'data' => '' ], t('id'), t('description'), t('owner') ),
          '#prefix' => '<br>',
        );
        $form['list'] = array_merge( $form['list'], $row );
        break;
      case 'general_settings':
        if ($this->testWebform($myentity->getWebformID())) {
          foreach($wfids as $key => $value ) {
            $options = array_merge($options,array($key=>$value));
          }

          $form['label'] = [
            '#type' => 'textfield',
            '#title' => $this->t('The name of Evaluation'),
            '#maxlength' => 255,
            '#default_value' => $myentity->label(),
            '#description' => $this->t("Label for the EvalWF."),
            '#required' => TRUE,
          ];
          $form['id'] = [
            '#type' => 'machine_name',
            '#default_value' => $myentity->id(),
            '#machine_name' => [
              'exists' => [$this, 'exist'],
            ],
            '#disabled' => !$myentity->isNew(),
            '#title' => 'id',
          ];

          $form['webform'] = array(
            '#type' => 'details',
            '#title' => t('Webform settings'),
            '#tree' => TRUE,
            '#open' => TRUE,
          );

          $form['webform']['webform_id'] = [
            '#type' => 'select',
            '#title' => $this->t('Webform_ID'),
            '#default_value' => $myentity->getWebformID(),
            '#description' => $this->t("The Webform which will be evaluated."),
            '#options' => $options,
            '#required' => TRUE,
            '#disabled' => TRUE,
          ];

          $form['webform']['confirmation_type'] = [
            '#type' => 'textfield',
            '#title' => $this->t('The selected Webform\'s confirmation type'),
            '#maxlength' => 255,
            '#default_value' => $webform->getSettings()['confirmation_type'],
            '#description' => $this->t("The selected Webform's confirmation type. It should be changed to url and set the url as below for automatic evaluation."),
            '#disabled' => TRUE,
          ];
          $form['webform']['confirmation_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('The selected Webform\'s confirmation url'),
            '#maxlength' => 255,
            '#default_value' => $webform->getSettings()['confirmation_url'],
            '#description' => $this->t("The selected Webform's confirmation url. It should be changed to evalwf/[webform:id]/[webform_submission:sid]  or add EvalWF_Evaluation handler for automatic evaluation."),
            '#disabled' => TRUE,
          ];
          $form['webform']['chsettings'] = [
            '#type' => 'link',
            '#title' => t('Change this settings'),
            '#url' => Url::fromRoute( 'entity.webform.settings_confirmation', ['webform' => $myentity->getWebformID()] ),
            '#attributes' => ['target' => '_blank'],
          ];

           $handlers = $webform->getHandlers()->getConfiguration();
           $handlerkey = null;
           $handlerenabled = false;
           foreach ($handlers as $key => $handler) {
             if ($handler['id'] == 'EvalWF_Evaluation') {
               $handlerkey = $key;
               $handlerenabled = $handlers[$handlerkey]['status'];
             }
           }
           $form['webform']['handler'] = [
             '#type' => 'textfield',
             '#title' => $this->t('The selected Webform\'s EvalWF_Evaluation handler'),
             '#maxlength' => 255,
             '#default_value' => ($handlerkey!=null && $handlerenabled ) ? $handlerkey . ' : ' . $handlers[$handlerkey]['id'] : '',
             '#description' => $this->t("The selected Webform's EvalWF_Evaluation handler should be added or set confirmation url to evalwf/[webform:id]/[webform_submission:sid] for automatic evaluation."),
             '#disabled' => TRUE,
           ];
           $form['webform']['chhandler'] = [
             '#type' => 'link',
             '#title' => t('Change handler settings'),
             '#url' => Url::fromRoute( 'entity.webform.handlers', ['webform' => $myentity->getWebformID()] ),
             '#attributes' => ['target' => '_blank'],
           ];

          $form['evalpage'] = array(
            '#type' => 'details',
            '#title' => t('Evaluation page settings'),
            '#tree' => TRUE,
            '#open' => TRUE,
          );
          $form['evalpage']['evaltitle'] = [
            '#type' => 'textfield',
            '#title' => $this->t('The title of evaluation page'),
            '#maxlength' => 255,
            '#default_value' => $myentity->getEvaluationTitle(),
            '#description' => $this->t("The title of evaluation page."),
          ];
          $form['evalpage']['incevaltitle'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Include title in content'),
            '#default_value' => $myentity->isIncludePTitle(),
            '#description' => $this->t("If it is checked the result page will include %title% in the content. (useful if your block design doesn't display titles)"),
          ];
          $form['evalpage']['evalintro'] = [
            '#type' => 'textarea',
            '#title' => $this->t('The intro text of the evaluation page'),
            '#default_value' => $myentity->getEvaluationIntro(),
            '#description' => $this->t("The intro text of the evaluation page."),
          ];
          $form['evalpage']['directjump'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Direct jump'),
            '#default_value' => $myentity->isDirectJump(),
            '#description' => $this->t("Nothing will display, just jump to conditional continue url."),
          ];
          $form['evalpage']['showevalpage'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Display evaluation result.'),
            '#default_value' => $myentity->isShowEvaluationPage(),
            '#description' => $this->t("After submisson it will display evaluation if direct jump haven't set."),
          ];
          $form['evalpage']['showall'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Display all available results.'),
            '#default_value' => $myentity->isShowAll(),
            '#description' => $this->t("Display all available results. If it's unchecked, at the evaluation page only the selected anwers and its points will be shown."),
          ];
          $form['evalpage']['renderasform'] = [
            '#type' => 'select',
            '#title' => $this->t('Formatting way:'),
            '#options' => [
              1 => $this->t('Display results as tables'),
              0 => $this->t('Use theme-ing... (%s)', ['%s'=>'evalw-resultpage.html.twig']),
            ],
            '#default_value' => EvalWFTxtHelper::BoolToInt($myentity->isRenderAsForm()),
            '#description' => $this->t("Select how to generate result page (Default: table design)."),
          ];

          $form['emailgroup'] = array(
            '#type' => 'details',
            '#title' => t('Email settings'),
            '#tree' => TRUE,
            '#open' => TRUE,
          );
          $form['emailgroup']['emailincevaltitle'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Include title in email body'),
            '#default_value' => $myentity->isIncludePTitle( true ),
            '#description' => $this->t("If it is checked the email body will include %title% in the content."),
          ];
          $form['emailgroup']['sendinemail'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Send results to email address.'),
            '#default_value' => $myentity->isSendInEmail(),
            '#description' => $this->t("When someone submitted, all the results will sent to the below specified email address."),
          ];
          $form['emailgroup']['email'] = [
            '#type' => 'email',
            '#title' => $this->t('The email address for collecting automatic evaluations.'),
            '#maxlength' => 255,
            '#default_value' => $myentity->getCollectorEmailAddress(),
            '#description' => $this->t("The email address for collecting automatic evaluations."),
          ];
          $form['emailgroup']['sendtouser'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Send results to user.'),
            '#default_value' => $myentity->isSendToUser(),
            '#description' => $this->t("It will send the result to user if user logged in."),
          ];
          $form['emailgroup']['emailrenderasform'] = [
            '#type' => 'select',
            '#title' => $this->t('Email formatting way:'),
            '#options' => [
              1 => $this->t('As tables'),
              0 => $this->t('Use theme-ing... (%s)', ['%s'=>'evalw-resultpage.html.twig']),
            ],
            '#default_value' => EvalWFTxtHelper::BoolToInt($myentity->isRenderAsForm(true)),
            '#description' => $this->t("Select how to generate email (Default: table design)."),
          ];
        }
        break;
    case 'elements_settings':
        if ($this->testWebform($myentity->getWebformID())) {
          $webform_elements = $webform->getElementsDecoded();
          $settings = !empty($this->entity->getElementsData()) ? $this->entity->getElementsData() : null;

          $form['elements'] = array(
            '#type' => 'details',
            '#title' => t('Form elements'),
            '#tree' => TRUE,
            '#open' => TRUE,
          );
          $form['elements'][] = $this->getElementsForm($webform_elements, $settings);
        }
        break;
    case 'conditions_settings':
        if ($this->testWebform($myentity->getWebformID())) {
          $form['conditions'] = array(
            '#type' => 'details',
            '#title' => t('Conditions'),
            '#id' => 'conditions',
            '#tree' => TRUE,
            '#open' => TRUE,
          );
          $form['conditions'][] = $this->getConditionsForm( $form_state );
        }
        break;
      default:
        $form['debug'] = [
          '#type' => 'markup',
          '#markup' => serialize($form).'<br>',
        ];
        break;
    }

    return $form;
  }

  /**
   *    This function sets the action buttons/link on the form
   *    I'd like to remove these actions on add.
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    switch ($form_state->getFormObject()->operation) {
      case 'add':
        $actions = array();
        break;
      case 'general_settings':
      case 'elements_settings':
      case 'conditions_settings':
        if (sizeof($this->error_elements_list)==0) {
          $actions = parent::actions($form, $form_state);
          $actions['saveexit'] = array(
            '#type' => 'submit',
            '#value' => $this
              ->t('Save & exit'),
            '#exit' => true,
            '#submit' => array(
              '::save',
            ),
            '#limit_validation_errors' => array(),
          );
        }
        else {
          $actions = array();
          $actions['cancel'] = array(
            '#type' => 'submit',
            '#value' => $this
              ->t('Cancel'),
            '#exit' => true,
            '#submit' => array(
              '::cancel',
            ),
            '#limit_validation_errors' => array(),
          );

        }
        break;
      default:
        $actions = parent::actions($form, $form_state);
        break;
    }
    return $actions;
  }

  /**
   *    Checkes that any evalwf settings entity with @param id is exists or not.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('evalwf')->getQuery()->condition('id', $id)->execute();
    return (bool) $entity;
  }

  /**
   *    Saves settings data to entity configuration.
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   */
  public function save(array $form, FormStateInterface $form_state) {
    $myentity = $this->entity;
    $op = $form_state->getTriggeringElement();
    $data = $form_state->getUserInput();

    switch ($form_state->getFormObject()->operation) {
      case 'general_settings':
        $myentity->setEvaluationTitle( $data['evalpage']['evaltitle'] );
        $myentity->setEvaluationIntro( $data['evalpage']['evalintro'] );
        $myentity->setDirectJump( $data['evalpage']['directjump'] );
        $myentity->setShowEvaluationPage( $data['evalpage']['showevalpage'] );
        $myentity->setShowAll( $data['evalpage']['showall'] );
        $myentity->setRenderAsForm( $data['evalpage']['renderasform'] );
        $myentity->setIncludePTitle( $data['evalpage']['incevaltitle'] );

        $myentity->setIncludePTitle( $data['emailgroup']['emailincevaltitle'], true );
        $myentity->setSendInEmail( $data['emailgroup']['sendinemail'] );
        $myentity->setCollectorEmailAddress( $data['emailgroup']['email'] );
        $myentity->setSendToUser( $data['emailgroup']['sendtouser'] );
        $myentity->setRenderAsForm( $data['emailgroup']['emailrenderasform'], true );
        break;
      case 'elements_settings':
        if ($myentity->id()==null) {
          $myentity->setId($myentity->getWebformID());
        }
        if ($myentity->label()==null) {
          $myentity->setLabel($myentity->getWebformID());
        }
        if (isset($data['elements']) && isset($data['elements'][0]) ) {
          $myentity->setElementsData( serialize( $data['elements'][0] ) );
        }
        else {
          $myentity->setElementsData( null );
        }
        break;
      case 'conditions_settings':
        if ($myentity->id()==null) {
          $myentity->setId($myentity->getWebformID());
        }
        if ($myentity->label()==null) {
          $myentity->setLabel($myentity->getWebformID());
        }
        if ( isset($data['conditions'][0]['table']) ) {
          foreach ( $data['conditions'][0]['table'] as &$row ) {
            if (isset($row['url']) && explode('/',trim($row['url']))[0]=='') {
              $row['url'] = 'internal:'.$row['url'];
            }
            if (isset($row['elseurl']) && explode('/',trim($row['elseurl']))[0]=='') {
              $row['elseurl'] = 'internal:'.$row['elseurl'];
            }
          }
        }
        $myentity->setConditionsData( serialize( $data['conditions'][0] ) );
        break;
      default:
        break;
    }

    $status = $myentity->save();

    if ( isset($op['#exit']) && ($op['#exit']) )
      $form_state->setRedirect('entity.evalwf.collection');
  }

  public function cancel(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.webform.collection');
  }

   /**
   *    Recursively creates form from webform elements (in tables)
   *    @param array  items     - the webform elements
   *    @param array  settings  - the elements saved values
   *    @returns form array
   */
  function getElementsForm( $items, $settings) {
    $elementsform = [];
    foreach ($items as $key => $item ) {
      if (isset($item['#type'])) {
        $plugin = $this->pluginManager->getPluginObjectFor($item['#type']);
        if (!empty($plugin)) {
          $elementsform = array_merge( $elementsform, $plugin->getSettingsForm( $item, $settings, $key ) );
        }
      }
    }
    return $elementsform;
  }

   /**
   *    Creates form for conditions (ajax table)
   *    @param FormStateInterface $form_state
   *    @returns form array
   */
  function getConditionsForm( FormStateInterface $form_state ) {
    $conditionsform['button'] = array(
      '#type' => 'button',
      '#id' => 'Add',
      '#name' => 'Add',
      '#value' => t('Add'),
      '#suffix' => '<br><br>',
      '#ajax' => [
        'callback' => [$this->entity, 'myAjaxCallback'],
        'disable-refocus' => TRUE,
        'event' => 'click',
        'wrapper' => 'table',
      ],
    );

    $conditionsform = array_merge( $conditionsform, $this->entity->CreateConditionsTable( $form_state, [] ) );
    return $conditionsform;
  }
/* *******************************************************
*   Trying to analyze webform elements, if there is not unique and/or numeric element-keys
*/
  function checkkey($k, $i, $optvalue = null) {
    if (isset($this->elements_list[$k])) {
      $this->elements_isunique = false;
      \Drupal::messenger()->addMessage( t('Not unique key: ').$k.($optvalue!=null?' - '.$optvalue:'').' ('.(isset($i['#title']) ? $i['#title'] : $k ).')', 'error' );
      return false;
    }
    if (is_numeric($k)) {
      $this->elements_isnumeric = true;
      \Drupal::messenger()->addMessage( t('Numeric key: ').$k.($optvalue!=null?' - '.$optvalue:'').' ('.(isset($i['#title']) ? $i['#title'] : $k ).')', 'error' );
      return false;
    }
    return true;
  }

  function checkelementslist( $webform_elements ) {

    foreach ( $webform_elements as $k => $i) {
      if ( ($i) && isset($i['#type']) ) {
        switch( $i['#type'] ) {
          case 'fieldset':
          case 'container':
          case 'details':
            if ( $this->checkkey($k, $i) ) {
              $this->elements_list = array_merge( $this->elements_list, array($k => (isset($i['#title']) ? $i['#title'] : $k )) );
              $this->checkelementslist($i);
            }
            break;
          case 'checkboxes':
          case 'radios':
          case 'select':
          case 'tableselect':
            if ( $this->checkkey($k, $i) ) {
              if (isset($i['#title'])) {
                $this->elements_list = array_merge( $this->elements_list, array($k => $i['#title']) );
              }
            }
            foreach($i['#options'] as $k => $optvalue ) {
              if ( $this->checkkey($k, $i, $optvalue) ) {
                $this->elements_list = array_merge( $this->elements_list, array($k => $optvalue) );
              }
            }
            break;
          default:
            if ( $this->checkkey($k, $i) ) {
              if (isset($i['#title'])) {
                $this->elements_list = array_merge( $this->elements_list, array($k => $i['#title']) );
              }
            }
            break;
        }
      }
    }
  }

  function testWebform( $id = null ) {
    $wfids = [$id => $id];
    if ($id==null) {
      $query = \Drupal::entityQuery('webform')->accessCheck(FALSE);
      $wfids = $query->execute();
    }

    $this->error_elements_list = [];
    foreach($wfids as $key => $value ) {
      $webform = Webform::load($key);
      $webform_elements = $webform->getElementsDecoded();

      $this->elements_list = [];
      $this->elements_isunique = true;
      $this->elements_isnumeric = false;

      $this->checkelementslist( $webform_elements );
      if (!($this->elements_isunique && !$this->elements_isnumeric)) $this->error_elements_list[] = $key;
    }
    return (empty($this->error_elements_list));
  }
}

?>
