<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Form\EvalWFConfigForm.php - Creates admin side forms to manage evaluation settings
*
* @author Tóthpál István
*
* @tutorial: "Creating a configuration entity type in Drupal 8" tutorial
*      @url: https://www.drupal.org/node/1809494
*/

namespace Drupal\evalwf\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the EvalWFForm.
 */
class EvalWFConfigForm extends EntityForm {

  /**
   *    Creates the form for evalwf settings
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   *    @returns form array
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $query = \Drupal::entityQuery('evalwf_config')->accessCheck(FALSE)->condition('id', 'evalwf_config');
    $result = $query->execute();
    if ($result) $this->entity = $this->entity->load('evalwf_config');

    $form['admin'] = [
      '#type' => 'details',
      '#title' => t('Admin settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['admin']['sendmail'] = [
      '#type' => 'checkbox',
      '#title' => t('Send email on admin evaluation?'),
      '#value' => $this->entity->isAdminSendMail(),
      '#description' => t('It will send email on admin evaluation, not just when user evaluated, If haven\'t already sent.'),
    ];
    $form['admin']['evalasuser'] = [
      '#type' => 'checkbox',
      '#title' => t('Evaluate as user on admin evaluation?'),
      '#value' => $this->entity->isEvalAsUser(),
      '#description' => t('It will create result as submitter user on admin evaluation, if haven\'t evaluated yet.'),
    ];

    $form['email'] = [
      '#type' => 'details',
      '#title' => t('Emailing settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['email']['mailer'] = [
      '#type' => 'select',
      '#title' => t('Select Mailer'),
      '#default_value' => [ $this->entity->getMailer() ],
      '#options' => $this->entity->getAvailableMailers(),
    ];
    $form['email']['sendername'] = [
      '#type' => 'textfield',
      '#title' => t('Sender Name'),
      '#value' => $this->entity->getSenderName(),
      '#description' => t('The Display name for From email address. Works with phpmail, default: %name.', [ '%name' => 'EvalWF at site name' ]),
    ];
    $form['email']['sender'] = [
      '#type' => 'email',
      '#title' => t('Sender email address'),
      '#value' => $this->entity->getSender(),
      '#description' => t('The From item of the mail. Works with phpmail, default: %from.', [ '%from' => 'site email' ]),
    ];
    $form['email']['subject'] = [
      '#type' => 'textfield',
      '#title' => t('Subject of emails'),
      '#value' => $this->entity->getSubject(),
      '#description' => t('Subject of the email. You can add @wfid and @sid to refer the current webform and submission.'),
    ];

    return $form;
  }


  /**
   *    Saves settings data to entity configuration.
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   */
  public function save(array $form, FormStateInterface $form_state) {
    $myentity = $this->entity;
    $data = $form_state->getUserInput();

    $myentity->setId('evalwf_config');
    $myentity->setLabel('evalwf_config');

    $myentity->setAdminSendMail( isset($data['admin']['sendmail']) ? $data['admin']['sendmail'] : false );
    $myentity->setEvalAsUser( isset($data['admin']['evalasuser']) ? $data['admin']['evalasuser'] : false );

    $myentity->setMailer( $data['email']['mailer'] );
    $myentity->setSenderName( $data['email']['sendername'] );
    $myentity->setSender( $data['email']['sender'] );
    $myentity->setSubject( $data['email']['subject'] );

    $status = $myentity->save();
    \Drupal::messenger()->addMessage( 'Saved.' );
  }

}

?>
