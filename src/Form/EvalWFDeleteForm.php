<?php
/**
*   The skeleton of this file is from "Creating a configuration entity type in Drupal 8" tutorial
*       @url: https://www.drupal.org/node/1809494
*
*   Drupal’s online documentation is © 2000-2020 by the individual contributors and can be used in accordance with the
*   Creative Commons License,  Attribution-ShareAlike 2.0.
*   PHP code is distributed under the GNU General Public License.
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
*   @modified by Tóthpál István for EvalWF module
*/

namespace Drupal\evalwf\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete an EvalWF.
 */

class EvalWFDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return parent::getDescription();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.evalwf.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[] = [
      '#type' => 'markup',
      '#markup' => $this->t( 'It will delete all saved data of this evaluation setting.'),
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];

    switch ($this->entity->getEntityTypeId()) {
      case 'evalwf':
        $wfid = $this->entity->getWebformId();
        $query = \Drupal::entityQuery('evalwf_evaluation')->accessCheck(FALSE)->condition('wfid', $wfid);
        $res = $query->execute();
        if (!empty($res)) {
          $form[] = [
            '#type' => 'markup',
            '#markup' => $this->t( 'These elements also will be deleted, because these are connected to this evaluation settings:' ),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          ];
          $form['items'] = [
              '#theme' => 'item_list',
              '#title' => $this->t( 'EvalWF Evaluations' ),
              '#items' => []
          ];
          foreach( $res as $eid) {
            $evaluation = \Drupal::entityTypeManager()->getStorage('evalwf_evaluation')->load($eid);
            $form['items']['#items'][] =
              $this->t( '( Id: %eid )  %ts : %wfid', array(
                '%eid' => $eid,
                '%wfid' => $evaluation->getWfId(),
                '%ts' => $evaluation->getTimeStamp()
              ));
          }
          $form[] = [
            '#type' => 'markup',
            '#markup' => $this->t( '%db piece(s)', array( '%db' => count($res) ) ),
            '#prefix' => '<p>',
            '#suffix' => '</p><BR>',
          ];
        }
        break;
      default:
        break;
    }
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('Entity %label has been deleted.', array('%label' => $this->entity->label())));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
?>
