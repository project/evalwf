<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2020-2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Form\EvalWFEvaluationForm.php - Creates admin side forms to manage evaluations
*
* @author Tóthpál István
*
* @tutorial: "Creating a configuration entity type in Drupal 8" tutorial
*      @url: https://www.drupal.org/node/1809494
*/

namespace Drupal\evalwf\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\Yaml\Yaml;

use Drupal\evalwf\Controller\EvalWFController;
use Drupal\evalwf\Helper\EvalWFTxtHelper;

/**
 * Form handler for the EvalWFForm.
 */
class EvalWFEvaluationForm extends EntityForm {

  /**
   *    Creates the form for evaluations
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   *    @returns form array
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    switch ($form_state->getFormObject()->operation) {
      case 'view':
        $form['evaluation'] = [
          '#type' => 'details',
          '#title' => t('Evaluation data'),
          '#open' => TRUE,
          '#tree' => TRUE,
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getId() .'<br>',
          '#prefix' => t('ID').': ',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getWfId(),
          '#prefix' => '<b>'.t('Webform').': ',
          '#suffix' => '</b><br>',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getSId(),
          '#prefix' => '<b>'.t('Submission').': ',
          '#suffix' => '</b><br><br>',
        ];

        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getTotal_Points().'<br><br>',
          '#prefix' => '<b>'.t('Total points').':</b> ',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => Yaml::dump(unserialize($this->entity->getElements_Points())),
          '#prefix' => '<b>'.t('Elements points').':</b> <pre>',
          '#suffix' => '</pre><br>',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => Yaml::dump(unserialize($this->entity->getConditions_Data())),
          '#prefix' => '<b>'.t('Conditions data').':</b> <pre>',
          '#suffix' => '</pre><br>',
        ];

        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => EvalWFTxtHelper::BoolToTxt($this->entity->isSent()).'<br>',
          '#prefix' => t('Sent in email').': ',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getSentTo().'<br><br>',
          '#prefix' => t('Sent to').': ',
        ];

        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getTimestamp().'<br>',
          '#prefix' => t('Timestamp').': ',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' => $this->entity->getUserName().'<br><br>',
          '#prefix' => t('User').': ',
        ];
        $form['evaluation'][] = [
          '#type' => 'markup',
          '#markup' =>  Yaml::dump($this->entity->getCheckSum()),
          '#prefix' => t('Checksums').': <pre>',
          '#suffix' => '</pre><br>',
        ];
      break;
      case 'result':
        $mycontroller = new EvalWFController;
        $wf = \Drupal::entityTypeManager()->getStorage('webform')->load($this->entity->getWfId());
        $wfs = \Drupal::entityTypeManager()->getStorage('webform_submission')->load($this->entity->getSId());
        $form = $mycontroller->content( $wf, $wfs, true );
        break;
      case 'list':
        $this->filter = \Drupal::request()->query->get('filter');
        $form[] = \Drupal::entityTypeManager()->getListBuilder('evalwf_evaluation')->renderfilter();
        $form[] = $this->BuildSubmissionsTable($this->filter);
        break;
      default:
        $form['debug'] = [
              '#type' => 'markup',
              '#markup' => serialize($form_state).'<br>',
        ];
      break;
    }

    return $form;
  }

  /**
   *    This function runs the evaluation with selected submission ( button name contains )
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   */
  function Evaluate( $form, $form_state){
    $op = $form_state->getTriggeringElement();
    if ($op && ($op['#value']=='Evaluate' || $op['#value']==(string)t('Evaluate'))) {
      $target = explode('-',$op['#name']);

      $wf = \Drupal::entityTypeManager()->getStorage('webform')->load($target[1]);
      $evalsettings = \Drupal\evalwf\Entity\EvalWF::LoadEntityByWebformID( $wf->id() );

      if ($evalsettings) {
        $mycontroller = new EvalWFController;
        $wfs = \Drupal::entityTypeManager()->getStorage('webform_submission')->load($target[2]);

        $resultform = $mycontroller->content( $wf, $wfs, true );

        if ($resultform && !empty($evalsettings->getElementsData())) {
          $button = array(
            '#type' => 'link',
            '#title' => t('View'),
            '#url' => \Drupal\Core\Url::fromRoute('entity.evalwf_evaluation.view_form', [ 'evalwf_evaluation' => $target[2] ] ),
          );
          \Drupal::messenger()->addMessage( t('The selected submission has evaluated...') );
          \Drupal::messenger()->addMessage( $button );
        }
      }
      else {
        $button = array(
          '#type' => 'link',
          '#title' => t('Add evaluation settings'),
          '#url' => \Drupal\Core\Url::fromRoute('entity.evalwf.withpath.settings_form', ['webform' => $wf->id(),] ),
        );
        \Drupal::messenger()->addMessage(
          t('No Evaluation was set to this webform... Please add evaluation settings first...'),
          'warning'
        );
        \Drupal::messenger()->addMessage( $button, 'warning' );
      }
    }
  }

  /**
   *    Builds a table with not evaluated submissions with filtering
   *    @param filter - the selected webform filter
   */
  function BuildSubmissionsTable($filter) {
    $query = \Drupal::entityQuery('evalwf_evaluation')->accessCheck(TRUE);
    $evaluations = $query->execute();

    $form['submissions-title'] = [
          '#type' => 'markup',
          '#markup' => '<h2>'.t('Available submissions:').'</h2>',
          '#prefix' => '<br><br>',
    ];
    $form['submissions'] = [
      '#type' => 'table',
      '#header' => [
        t('ID'),
        t('Webform'),
        t('Last changed'),
        t('User'),
        t('Operations'),
      ],
    ];
    $emptysize = count($form['submissions']);

    if ($filter)  {
      $query = \Drupal::entityQuery('webform_submission')->accessCheck(FALSE)->condition('webform_id', $filter)->sort('changed','DESC');
    }
    else {
      $query = \Drupal::entityQuery('webform_submission')->accessCheck(FALSE)->sort('changed','DESC');
    }
    $submissions = $query->execute();

    foreach ( $submissions as $key => $value) {
      if (!in_array($key,$evaluations)) {
        $webform_submission = \Drupal\webform\entity\WebformSubmission::load($key);
        $form['submissions'][$key] = [
          'id' => [ '#type' => 'markup', '#markup' => $value, ],
          'webform' => [ '#type' => 'markup', '#markup' => $webform_submission->bundle(), ],
          'changed' => [ '#type' => 'markup', '#markup' => date('Y-m-d H:i:s',$webform_submission->getChangedTime()) ],
          'user' => [ '#type' => 'markup', '#markup' => ($webform_submission->getOwner() ? $webform_submission->getOwner()->getAccountName() : '' ), ],
          'operations' => [
            '#access' => \Drupal::currentUser()->hasPermission('administer evalwf settings') || (\Drupal::currentUser()->id()==$webform_submission->getOwnerId()),
            '#type' => 'submit',
            '#value' => t('Evaluate'),
            '#name' => 'Eval-'.$webform_submission->bundle().'-'.$key,
            '#submit' => array(
              '::Evaluate',
            ),
          ],
        ];
      }
    }

    if ( count($form['submissions']) <= $emptysize ) {
      $form = null;
    }
    return $form;
  }

  /**
   *    This function sets the action buttons/link on the form
   *    I'd like to remove these actions on add.
   *    @param form       - the current Form object
   *    @param form_state - the current FormStateInterface object
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    switch ($form_state->getFormObject()->operation) {
      case 'view':
      case 'list':
      case 'result':
        $actions = array();
        break;
      default:
        $actions = parent::actions($form, $form_state);
        break;
    }
    return $actions;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filter = $form_state->getUserInput()['filter_table'][0]['filter'];
    $query = \Drupal::request()->query->all();
    if (empty($filter) && isset($query['filter']))
      unset($query['filter']);
    else
      $query['filter'] = $filter;
    $form_state->setRedirect(
      $this->getRouteMatch()->getRouteName(),
      $this->getRouteMatch()->getRawParameters()->all(),
      ['query' => $query]
    );
  }
}

?>
