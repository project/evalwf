<?php
/**
*   The skeleton of this file is from "Creating a configuration entity type in Drupal 8" tutorial
*       @url: https://www.drupal.org/node/1809494
*
*   Drupal’s online documentation is © 2000-2020 by the individual contributors and can be used in accordance with the
*   Creative Commons License,  Attribution-ShareAlike 2.0.
*   PHP code is distributed under the GNU General Public License.
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
*   @modified by Tóthpál István for EvalWF module
*/

namespace Drupal\evalwf\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;

/**
 * Builds the form to delete an EvalWFEvaluation.
 */

class EvalWFEvaluationDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', array('%name' => !empty($this->entity->label()) ? $this->entity->label() : '' ));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.evalwf_evaluation.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('Entity %label has been deleted.', array('%label' => !empty($this->entity->label()) ? $this->entity->label() : '')));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
?>
