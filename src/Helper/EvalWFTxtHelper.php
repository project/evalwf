<?php
/**
*    This file is part of EvalWF Module - Automatic evaluation of forms was made by webform module.
*    Copyright (C) 2023  University of Szeged
*
*    This EvalWF Module is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Foobar is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*    it under the terms of the GNU General Public License as published by
*
* @file \Drupal\evalwf\Helper\EvalWFTxtHelper.php - converting to String and/or manipulating Strings for EvalWF
*
* @author Tóthpál István
*/

namespace Drupal\evalwf\Helper;


class EvalWFTxtHelper {

  // return $b ? 'true' : 'false';
  public static function BoolToTxt( $b ) {
    return var_export( boolval($b), 1 );
  }

  public static function BoolToInt( $b ) {
    return $b ? 1 : 0;
  }

}

?>
